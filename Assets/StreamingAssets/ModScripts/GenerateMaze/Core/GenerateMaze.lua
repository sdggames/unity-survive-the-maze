-- Defines a function that creates a random maze.
function populateMaze (maze)
	local x, y, i, j

	x = maze.MazeSize.x
	y = maze.MazeSize.y

	-- Do the border (may not be needed)
	for i=0,x-1 do
		CreateWall(i,0, maze)
		CreateWall(i,y-1, maze)
	end
	for j=1,y-2 do
		CreateWall(0,j, maze)
		CreateWall(x-1,j, maze)
	end

	-- Fill it in.
	local placementThreshold = 25
	for i=1,x-2 do
		for j=1,y-2 do
				if (i % 2 == 0 and j % 2 == 0) then
				local a = math.random( 0,1 ) < .5 and 0 or 1
				local b = a ~= 0 and 0 or 1
				if (math.random( 0,100 ) > placementThreshold) then
					CreateWall(i,j, maze)
				end
				if (math.random( 0,100 ) > placementThreshold) and (a ~= 0 or b ~= 0) then
					CreateWall(i+a,j+b, maze)
				end
			end
		end
	end
end

function CreateWall(x, y, maze)
	if (maze[x, y].IsVisible) then
		maze[x, y].WallExists = true
	end
end
