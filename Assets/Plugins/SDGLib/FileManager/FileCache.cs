﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
#if !UNITY_EDITOR && (UNITY_WEBGL || UNITY_ANDROID)
using UnityEngine.Networking;
#endif

[Serializable]
public class FileCache
{
    /// <summary>
    /// Create a new ScriptCache instance for the specified target directory.
    /// The AiScriptCache manages files from StreamingAssets/AiScripts
    /// The ModScriptCache manages files from StreamingAssets/ModScripts
    /// </summary>
    /// <param name="cacheMode"></param>
    public FileCache(string folderName) => this.FolderName = folderName;


    /// <summary>
    /// Local, cached copies of each loaded file that exist in RAM.
    /// </summary>
    public Dictionary<string, string> FileContents;

    /// <summary>
    /// Delegate will return when all file operations have completed.
    /// </summary>
    public delegate void LoadComplete();

    /// <summary>
    /// True if all files are loaded and ready to be returned, false otherwise.
    /// </summary>
    public bool FilesAreLoaded = false;

    /// <summary>
    /// The name of this folder in the file system.
    /// </summary>
    private string FolderName;

    /// <summary>
    /// All files and folders in the directory.
    /// </summary>
    private FileManafest Manafest = null;

    /// <summary>
    /// Parses the given directory, and then loads every file in the directory and
    /// subdirectories. Equivalent to calling BuildDirectory, then LoadFiles.
    /// </summary>
    /// <returns></returns>
    public IEnumerator BuildAndGenerateAllFiles()
    {
        IEnumerator build = BuildDirectory();
        while (build.MoveNext())
            yield return build.Current;

        IEnumerator generate = LoadFiles();
        while (generate.MoveNext())
            yield return generate.Current;
    }

    /// <summary>
    /// Coroutine to load each manafest file in the directory and subdirectory.
    /// Must be called before LoadFiles is called.
    /// </summary>
    /// <returns></returns>
    public IEnumerator BuildDirectory()
    {
        this.FilesAreLoaded = false;
        this.Manafest = new FileManafest();

        IEnumerator loadManafests = this.Manafest.LoadOrBuildManafestsRecursively(this.FolderName);
        while (loadManafests.MoveNext()) yield return loadManafests.Current;
    }

    /// <summary>
    /// Loads all selected scripts into memory from disk or from over the network.
    /// </summary>
    /// <returns></returns>
    public IEnumerator LoadFiles()
    {
        Assert.IsNotNull(this.Manafest, "Error: Attempted to load a file before the file manafest was generated!");

        this.FileContents = new Dictionary<string, string>();

        foreach (string fileName in this.GetFullFileNames(this.Manafest, this.FolderName, new List<string>()))
        {
            string dataPath = Path.Combine(Application.streamingAssetsPath, fileName);

#if !UNITY_EDITOR && (UNITY_WEBGL || UNITY_ANDROID)
            using (UnityWebRequest request = UnityWebRequest.Get(dataPath))
            {
                yield return request.SendWebRequest();

                if (request.isNetworkError)
                    throw new FileNotFoundException("File " + fileName + " was listed in manafest file, but doesn't actually exist on disk!");
                else
                    this.FileContents.Add(fileName.ToLower(), request.downloadHandler.text);
            }
#else
            using (StreamReader streamReader = File.OpenText(dataPath))
                this.FileContents.Add(fileName.ToLower(), streamReader.ReadToEnd());
#endif
        }

        this.FilesAreLoaded = true;
        yield return null;
    }

    /// <summary>
    /// Returns an array containing the absolute paths to each of the directories 
    /// in the cache. (note: folders containing only other folders are not explicitly enumerated)
    /// </summary>
    /// <returns></returns>
    public string[] BuildPaths() => this.BuildPaths(this.Manafest, this.FolderName, new List<string>() { this.FolderName }).ToArray();
    private List<string> BuildPaths(FileManafest currentDir, string currentPath, List<string> pathList)
    {
        // Add the current directory if it contains files.
        if (currentDir.files.Count > 0)
            pathList.Add(currentPath);

        // Call down into the next subdirectory.
        foreach (string subDir in currentDir.subDirectories)
            if (currentDir.subDirectoryContents[subDir].IsDirectoryActive)
                this.BuildPaths(currentDir.subDirectoryContents[subDir], currentPath + "/" + subDir, pathList);

        return pathList;
    }

    /// <summary>
    /// Looks for a file in the current mod folders and returns true if it exists.
    /// </summary>
    /// <param name="fullPath"></param> The path relative to the file cache's root directory.
    /// <returns></returns>
    public bool FileExists(string fullPath)
    {
        string[] fullDirectory = fullPath.Split('/');
        return this.FileExists(fullDirectory.ToArray(), this.Manafest);
    }
    private bool FileExists(string[] fullPath, FileManafest currentDirectory)
    {
        if (fullPath.Length == 1)
        {
            // We're looking for a folder.
            foreach (string file in currentDirectory.files)
                if (fullPath[0].Equals(file, StringComparison.OrdinalIgnoreCase))
                    return true;
        }
        else
        {
            // We're looking for a folder.
            foreach (string subdir in currentDirectory.subDirectories)
                if (fullPath[0].Equals(subdir, StringComparison.OrdinalIgnoreCase))
                {
                    FileManafest manafest = currentDirectory.subDirectoryContents[subdir];
                    if (manafest.IsDirectoryActive)
                        return this.FileExists(fullPath.Skip(1).ToArray(), manafest);
                }
        }
        // No valid match.
        return false;
    }

    /// <summary>
    /// Returns the contents of the requested file. Does not provide any error checking,
    /// call FileExists first to ensure that data will be returned.
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public string LoadFile(string filePath) => this.FileContents[(this.FolderName + "/" + filePath).ToLower()];

    /// <summary>
    /// Returns the file contents deserialized into an object. Does not privide error checking,
    /// call FileExists first to ensure that data will be returned.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public T LoadFile<T>(string filePath) => JsonUtility.FromJson<T>(LoadFile(filePath));

    /// <summary>
    /// Helper function to generate a list of all file names with full paths.
    /// </summary>
    /// <param name="currentDir"></param>
    /// <param name="currentPath"></param>
    /// <param name="fileList"></param>
    /// <returns>fileList (same object that was passed in, returned in case you want to assign it in a single line)</returns>
    private List<string> GetFullFileNames(FileManafest currentDir, string currentPath, List<string> fileList)
    {
        // Add the current directory if it contains files.
        foreach (string file in currentDir.files)
            fileList.Add(currentPath + "/" + file);

        // Call down into the next subdirectory.
        foreach (string subDir in currentDir.subDirectories)
            if (currentDir.subDirectoryContents[subDir].IsDirectoryActive)
                this.GetFullFileNames(currentDir.subDirectoryContents[subDir], currentPath + "/" + subDir, fileList);

        return fileList;
    }
}
