﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// Small data structure that will contain a list of all files and subdirectories
/// in a particular directory. Serializable so we can save it as a Json.
/// 
/// P.S. Not a misspelling.  In some popular video games, *mana* is another word for magic.
/// Since we are doing video games, we are going to use manafests instead of manifests.
/// Also, there's apparently already a file convention for .manifest files, and this ain't it.
/// </summary>
[Serializable]
public class FileManafest
{
    /// <summary>
    /// File path to each of the subdirectories in this directory.
    /// </summary>
    public List<string> subDirectories = new List<string>();

    /// <summary>
    /// File path to each of the files in this directory.
    /// </summary>
    public List<string> files = new List<string>();

    /// <summary>
    /// Flag to indicate whether the files in this directory should be loaded or passed over. NonSerialized.
    /// </summary>
    [NonSerialized] public bool IsDirectoryActive = true;

    /// <summary>
    /// Contains references to the FileManafests below me. It's manafests all the way down!
    /// </summary>
    public Dictionary<string, FileManafest> subDirectoryContents = new Dictionary<string, FileManafest>();

#if UNITY_EDITOR || !(UNITY_WEBGL || UNITY_ANDROID)
    /// <summary>
    /// Builds the manafest from file synchronosly.
    /// FUNCTION ONLY WORKS ON PC! USE LoadOrBuildManafest INSTEAD IF YOU ARE UNSURE.
    /// </summary>
    /// <param name="targetDirectory"></param>
    public void BuildManafest(string targetDirectory)
    {
        string fullPath = Path.Combine(Application.streamingAssetsPath, targetDirectory);

        string[] fileEntries = Directory.GetFiles(fullPath);
        foreach (string fileName in fileEntries)
            if (!fileName.Contains(".meta") && !fileName.Contains("manafest")) // Don't add these, it would be way too meta...
                this.files.Add(fileName.Replace(fullPath + "\\", "").Replace(fullPath + "/", ""));

        string[] subdirectoryEntries = Directory.GetDirectories(fullPath);
        foreach (string subdirectory in subdirectoryEntries)
            this.subDirectories.Add(subdirectory.Replace(fullPath + "\\", "").Replace(fullPath + "/", ""));
    }

    /// <summary>
    /// Builds the manafest files for the entire directory structure synchronosly.
    /// FUNCTION ONLY WORKS ON PC! USE LoadOrBuildManafest INSTEAD IF YOU ARE UNSURE.
    /// </summary>
    /// <param name="targetDirectory"></param>
    public void BuildManafestsRecursively(string targetDirectory)
    {
        BuildManafest(targetDirectory);

        foreach (string subdir in this.subDirectories)
        {
            this.subDirectoryContents.Add(subdir, new FileManafest());

            // Iterate on all sub-directories.
            this.subDirectoryContents[subdir].BuildManafestsRecursively(Path.Combine(targetDirectory, subdir));            
        }
    }
#endif

    /// <summary>
    /// Loads the current manafest from file or builds it anew.
    /// </summary>
    /// <param name="targetDirectory"></param>
    /// <param name="loadComplete"></param>
    /// <returns></returns>
    public IEnumerator LoadOrBuildManafest(string targetDirectory)
    {
#if !UNITY_EDITOR && (UNITY_WEBGL || UNITY_ANDROID)
        IEnumerator loadAsset = FileManager.LoadStreamingAsset(Path.Combine(targetDirectory, "manafest"), this);
        while (loadAsset.MoveNext()) yield return loadAsset.Current;
#else
        BuildManafest(targetDirectory);
        yield return null;
#endif
    }

    /// <summary>
    /// Loads or builds the manafest files for the entire directory structure.
    /// </summary>
    /// <param name="targetDirectory"></param>
    /// <returns></returns>
    public IEnumerator LoadOrBuildManafestsRecursively(string targetDirectory)
    {
        // Get the contents of this manafest.
        IEnumerator loadAsset = LoadOrBuildManafest(targetDirectory);
        while (loadAsset.MoveNext()) yield return loadAsset.Current;

        foreach (string subdir in this.subDirectories)
        {
            this.subDirectoryContents.Add(subdir, new FileManafest());

            // Iterate on all sub-directories.
            IEnumerator buildCache = this.subDirectoryContents[subdir].LoadOrBuildManafestsRecursively(Path.Combine(targetDirectory, subdir));
            while (buildCache.MoveNext()) yield return buildCache.Current;
        }
    }
}
