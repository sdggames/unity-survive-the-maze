﻿using CommandTerminal;

namespace Debugging
{
    public abstract class DebugMenuPage
    {
        /// <summary>
        /// The name of the menu, will be displayed in a list of menu tabs before the menu is selected.
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// Draws the menu buttons. Each menu page will define this on its own.
        /// UNITY MAY CALL THIS MORE THAN ONCE PER FRAME!
        /// Menus are meant to draw things, logic should be put in a controller class or singleton.
        /// </summary>
        public abstract void Draw();

        /// <summary>
        /// Function that runs any number of commands one after another.
        /// </summary>
        /// <param name="commands"></param>
        /// <returns></returns>
        protected void RunCommand(params string[] commands)
        {
            // Process each command in order. Skip one frame between each.
            foreach (string command in commands)
            {
                Terminal.Shell.RunCommand(command);
            }
        }
    }
}
