﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Assertions;
using System.Linq;

namespace Debugging
{
    public class DebugMenuManager : MonoBehaviour
    {
        // Menus
        private readonly List<DebugMenuPage> menuPages = new List<DebugMenuPage>();
        private DebugMenuPage activeMenu = null;
        private bool debugMenuActive;

        // FPS Counter
        private DebugMenuPage fpsMenu = null;
        private readonly FPSCounter fps = new FPSCounter();
        private bool fpsMenuActive;

        private void Start()
        {
            this.fpsMenu = new FpsMenu();

            this.RegisterAllSubmenus();
        }

        private Vector2 Scroll;

        /// <summary>
        ///  For use by the FPS menu (if activated)
        /// </summary>
        private void Update() => this.fps.UpdateFps();

        private void OnGUI()
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("…"))
                this.ToggleMenuVisibility();

            if (this.debugMenuActive)
            {
                foreach (DebugMenuPage menu in this.menuPages)
                {
                    if (GUILayout.Button(menu.Name))
                    {
                        this.activeMenu = menu;
                    }
                }

                if (GUILayout.Button(this.fpsMenu.Name))
                    this.ToggleFrameCounterVisibility();

                GUILayout.EndHorizontal();

                // Wrap everything in the designated GUI Area
                GUI.Box(new Rect(0, 30, Screen.width / 1.5f, Screen.height - 30), "");
                GUILayout.BeginArea(new Rect(0, 30, Screen.width / 1.5f, Screen.height - 30));
                this.Scroll = GUILayout.BeginScrollView(this.Scroll);

                GUILayout.Space(32);
                this.activeMenu?.Draw();

                GUILayout.EndScrollView();
                GUILayout.EndArea();
            }
            else
                GUILayout.EndHorizontal();

            if (this.fpsMenuActive)
                this.fpsMenu?.Draw();
        }

        /// <summary>
        /// Find every derived class from SubMenuPage and create exactly one of each.
        /// </summary>
        private void RegisterAllSubmenus()
        {
            foreach (var DebugMenu in from Assembly assembly in AppDomain.CurrentDomain.GetAssemblies()
                                      where assembly.GetName().Name != "Plugins" && !assembly.GetName().Name.Contains("Unity")
                                      from Type type in assembly.GetTypes()
                                      where type.IsSubclassOf(typeof(DebugMenuPage))
                                      let DebugMenu = (DebugMenuPage)Activator.CreateInstance(type)
                                      select DebugMenu)
            {
                this.RegisterMenu(DebugMenu);
            }
        }

        /// <summary>
        /// Adds a menu page to the Debugging Menu tab list
        /// </summary>
        /// <param name="menuPage"></param>
        private void RegisterMenu(DebugMenuPage menuPage)
        {
            this.menuPages.Add(menuPage);
            if (this.activeMenu == null)
            {
                this.activeMenu = this.menuPages[0];
            }
        }

        /// <summary>
        /// Activates or deactivates the menu visibility.
        /// </summary>
        public void ToggleMenuVisibility()
        {
            this.debugMenuActive = !this.debugMenuActive;
            Assert.AreNotEqual(0, this.menuPages.Count, "Cannot open the debugging menu, no pages are defined!");
        }

        /// <summary>
        /// Activates or deactivates the frame counter visibility.
        /// </summary>
        public void ToggleFrameCounterVisibility() => this.fpsMenuActive = !this.fpsMenuActive;
    }
}
