﻿using UnityEngine;

namespace Debugging
{
    internal class FpsMenu : DebugMenuPage
    {
        /// <summary>
        /// The name of the menu, will be displayed in a list of menu tabs before the menu is selected.
        /// </summary>
        public override string Name => "Frame Counter";

        /// <summary>
        /// Draws the menu buttons. Each menu page will define this on its own.
        /// </summary>
        public override void Draw()
        {
            GUI.Box(new Rect(Screen.width - 100, 0, 100, 50),
                "High: " + FPSCounter.HighestFPS.ToString() + "\n" +
                "Avg:  " + FPSCounter.AverageFPS.ToString() + "\n" +
                "Low:  " + FPSCounter.LowestFPS.ToString());
        }
    }
}
