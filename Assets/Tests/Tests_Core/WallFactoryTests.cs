﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using Tests;

[Category("Static Tests")]
internal class WallFactoryTests : ManagedTest
{
    internal Transform parentTransform;
    private Wall wallTemplate;

    [SetUp]
    public void SetUp()
    {
        // Set our parent transform.
        this.parentTransform = new GameObject("ParentTransform").transform;
        WallFactory.SetFreeObjectParent(this.parentTransform);
        this.SetFactoryTemplate();
    }

    [Test]
    public void WallFactoryPrefabPassFail()
    {
        WallFactory.Reset();
        try
        {
            _ = WallFactory.GetWall();
            Assert.Fail("We should not have been able to get this wall segment.");
        }
        catch (NullPrefabException)
        {
            // We want this one.
        }
        this.SetFactoryTemplate();
        Wall wall = WallFactory.GetWall(); // Should not throw an exception

        WallFactory.Return(wall);
        WallFactory.Reset();
        try
        {
            _ = WallFactory.GetWall();
            Assert.Fail("We should not have been able to get this wall segment.");
        }
        catch (NullPrefabException)
        {
            // We want this one.
        }
    }

    [Test]
    public void WallFactoryCreateAtLocation()
    {
        // Get a Wall from the factory.
        for (int i = 0; i < 1000; i++)
        {
            Vector2Int wallLocation = this.RandomVector2Int;
            Wall wall = WallFactory.GetWall(wallLocation, null);
            Assert.AreEqual(wallLocation, new Vector2Int((int)wall.transform.position.x, (int)wall.transform.position.z));

            // Cleanup.
            WallFactory.Return(wall);
        }
        WallFactory.Reset();
    }

    [Test]
    public void WallFactoryGetReturn()
    {
        Assert.AreEqual(this.parentTransform.childCount, 0);

        // Get a Wall from the factory.
        Wall wall = WallFactory.GetWall();
        Assert.AreEqual(this.parentTransform.childCount, 0);

        // Return the Wall.
        WallFactory.Return(wall);
        Assert.AreEqual(this.parentTransform.childCount, 1);
        Assert.AreSame(this.parentTransform.GetComponentInChildren<Wall>(true), wall);

        // Get the same object.
        Wall newWall = WallFactory.GetWall();
        Assert.AreSame(wall, newWall);
        Assert.AreEqual(this.parentTransform.childCount, 0);

        // Return the Wall.
        WallFactory.Return(newWall);
        Assert.AreEqual(this.parentTransform.childCount, 1);

        // Clear out the WallFactory before the next test.
        WallFactory.Reset();
        Assert.AreEqual(this.parentTransform.childCount, 0);
    }

    [Test]
    public void WallFactoryGetMultiple()
    {
        List<Wall> Walls = new List<Wall>();
        this.SetFactoryTemplate();

        // Get 1000 new objects, make sure that there are no memory leaks, etc.
        for (int i = 0; i < 1000; i++)
        {
            Walls.Add(WallFactory.GetWall());
        }
        foreach (Wall wall in Walls)
        {
            WallFactory.Return(wall);
        }

        // Clear out the WallFactory before the next test.
        WallFactory.Reset();
    }

    [Test]
    public void WallFactoryEnableDisableObject()
    {
        // Get a Wall from the factory.
        Wall wall = WallFactory.GetWall();
        Assert.IsTrue(wall.isActiveAndEnabled);

        // Return the Wall.
        WallFactory.Return(wall);
        Assert.IsFalse(wall.isActiveAndEnabled);

        // Get the same object.
        _ = WallFactory.GetWall();
        Assert.IsTrue(wall.isActiveAndEnabled);

        // Clear out the WallFactory before the next test.
        WallFactory.Return(wall);
        WallFactory.Reset();
    }

    [TearDown]
    public void TearDown()
    {
        Object.DestroyImmediate(this.wallTemplate.gameObject);
        Object.DestroyImmediate(this.parentTransform.gameObject);
    }

    private void SetFactoryTemplate()
    {
        if (this.wallTemplate != null)
        {
            Object.DestroyImmediate(this.wallTemplate.gameObject);
        }
        GameObject gameObject = new GameObject("Wall Template");
        _ = gameObject.AddComponent<BoxCollider>();
        this.wallTemplate = gameObject.AddComponent<Wall>();
        WallFactory.WallPrefab = this.wallTemplate;
    }
}
