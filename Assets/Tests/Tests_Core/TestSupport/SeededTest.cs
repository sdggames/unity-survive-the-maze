﻿using NUnit.Framework;
using NUnit.Framework.Interfaces;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Tests
{
    public class SeededTest
    {
        private Scene testScene;

        /// <summary>
        /// Returns a random Vector2 between (0, 0) (inclusive) and (100, 100) (exclusive)
        /// </summary>
        public Vector2 RandomVector2 => new Vector2(Random.Range(0, 100), Random.Range(0, 100));

        /// <summary>
        /// Returns a random Vector2Int between (1, 1) (inclusive) and (100, 100) (inclusive)
        /// </summary>
        public Vector2Int RandomVector2Int => new Vector2Int(Random.Range(1, 101), Random.Range(1, 101));

        [OneTimeSetUp]
        public void LoadSeedForTestSuite() => RandomSeedGenerator.LoadSeed();

        [SetUp]
        public void GenerateNextSeededTest()
        {
            int seed = RandomSeedGenerator.NextSeed;
            Random.InitState(seed);
            Debug.Log("Random Seed: " + seed.ToString());

            if (!Application.isBatchMode)
                this.testScene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);
        }

        [TearDown]
        public void ObjectDeletionCheck()
        {
            if (!Application.isBatchMode)
                foreach (GameObject o in this.testScene.GetRootGameObjects())
                {
                    if (TestContext.CurrentContext.Result.Outcome.Status.Equals(TestStatus.Failed))
                        Object.DestroyImmediate(o);
                    else
                        Debug.LogError("The test left objects in the scene. " + o.name);
                }
        }

        /// <summary>
        /// Checks if the UT is being run in batch mode (Command Prompt). If so, attempts
        /// to save the random seed for persistance across runs.
        /// </summary>
        [OneTimeTearDown]
        public void SaveSeededTestSuiteResults() => RandomSeedGenerator.SaveSeed();
    }
}
