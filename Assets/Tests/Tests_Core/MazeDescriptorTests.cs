﻿using NUnit.Framework;
using Tests;
using UnityEngine;

public class MazeDescriptorTests : SeededTest
{
    [Test]
    public void MazeDescriptor_InitTest()
    {
        Vector2Int mazeSize = this.RandomVector2Int;

        MazeDescriptor mazeDescriptor = new MazeDescriptor(mazeSize);

        Assert.IsNotNull(mazeDescriptor);
        Assert.IsNotNull(mazeDescriptor.Items);
        Assert.IsNotNull(mazeDescriptor.Mortals);

        Assert.AreEqual(mazeSize.x, mazeDescriptor.MazeSize.x);
        Assert.AreEqual(mazeSize.y, mazeDescriptor.MazeSize.y);
    }

    [Test]
    public void MazeDescriptor_InitCellsTest()
    {
        Vector2Int mazeSize = this.RandomVector2Int;

        MazeDescriptor mazeDescriptor = new MazeDescriptor(mazeSize);

        for (int x = 0; x < mazeDescriptor.MazeSize.x; x++)
            for (int y = 0; y < mazeDescriptor.MazeSize.y; y++)
        {
            Assert.IsNotNull(mazeDescriptor[x,y]);
            Assert.Zero(mazeDescriptor[x, y].ItemsInCell.Count);
            Assert.Zero(mazeDescriptor[x, y].MortalsInCell.Count);
            Assert.IsNotNull(mazeDescriptor[x, y].ItemsInCell);
            Assert.IsNotNull(mazeDescriptor[x, y].MortalsInCell);
        }
    }

    [Test]
    public void ItemDescriptor_InitTest()
    {
        ItemDescriptor itemDescriptor = new ItemDescriptor();

        Assert.IsNotNull(itemDescriptor);
        // No objects to null-check, simple types always have a value.
    }

    [Test]
    public void MortalDescriptor_InitTest()
    {
        MortalDescriptor mortalDescriptor = new MortalDescriptor();

        Assert.IsNotNull(mortalDescriptor);
        Assert.IsNull(mortalDescriptor.HeldItem1);
        Assert.IsNull(mortalDescriptor.HeldItem2);
    }

    [Test]
    public void MazeCellDescriptor_InitTest()
    {
        MazeCellDescriptor mazeCellDescriptor = new MazeCellDescriptor();

        Assert.IsNotNull(mazeCellDescriptor);
        Assert.True(mazeCellDescriptor.IsVisible);
        Assert.False(mazeCellDescriptor.WallExists);
        Assert.IsNotNull(mazeCellDescriptor.ItemsInCell);
        Assert.IsNotNull(mazeCellDescriptor.MortalsInCell);
        Assert.Zero(mazeCellDescriptor.ItemsInCell.Count);
        Assert.Zero(mazeCellDescriptor.MortalsInCell.Count);
    }
}
