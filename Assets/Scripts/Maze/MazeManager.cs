﻿using Callouts;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

internal class MazeManager : MonoBehaviour
{
    private static MazeManager Me;

    [Serializable]
    public class MortalPrefab
    {
        public MortalDescriptor mortalDescriptor;
        public Transform prefab;
    }

    [Serializable]
    public class ItemPrefab
    {
        public ItemDescriptor itemDescriptor;
        public Transform prefab;
    }

    [SerializeField] internal MazeLoadData MazeToLoad;
    [SerializeField] internal Transform MazeParent;
    [SerializeField] internal Transform InactiveWallParent;
    [SerializeField] internal Transform Floor;
    [SerializeField] internal List<MortalPrefab> mortalPrefabs;
    [SerializeField] internal List<ItemPrefab> itemPrefabs;
    [SerializeField] internal List<Mortal> mortals;
    [SerializeField] internal List<Item> items;

    /// <summary>
    /// The maze data which represents every object on screen.
    /// </summary>
    internal MazeDescriptor Maze { get; private set; } = new MazeDescriptor(new Vector2Int(50, 50));

    /// <summary>
    /// The size in grid spaces of the current maze.
    /// </summary>
    internal static Vector2Int MazeSize => Me.Maze.MazeSize;
    
    private Dictionary<string, Transform> prefabs = new Dictionary<string, Transform>();
    private Wall[,] WallObjects = new Wall[50, 50];
    private FileCache mazeFiles;

    /// <summary>
    /// Set up the Wall Factory and the Populate Maze callout.
    /// </summary>
    internal void Awake()
    {
        if (Me == null)
            Me = this;
        else
            throw new SingletonReInstantiatedException("There is already a MazeManager in the scene!");

        WallFactory.SetFreeObjectParent(this.InactiveWallParent);

        if (this.MazeParent == null)
        {
            this.MazeParent = this.transform;
            throw new ObjectNotFoundException("MazeManager has no reference to the Maze Object!");
        }
        if (this.InactiveWallParent == null)
        {
            this.InactiveWallParent = this.transform;
            throw new ObjectNotFoundException("MazeManager has no Inactive Wall Segment Parent!");
        }
        if (this.Floor == null)
            throw new ObjectNotFoundException("MazeManager has no reference to the Floor object!");
    }

    internal void Start()
    {
        this.mazeFiles = new FileCache("Scenarios");
        _ = this.StartCoroutine(this.mazeFiles.BuildAndGenerateAllFiles());
        foreach (MortalPrefab prefab in this.mortalPrefabs)
            this.prefabs.Add(prefab.prefab.name, prefab.prefab);
        foreach (ItemPrefab prefab in this.itemPrefabs)
            this.prefabs.Add(prefab.prefab.name, prefab.prefab);
    }

    /// <summary>
    /// Create a maze of the specified size.
    /// </summary>
    internal void CreateMaze(Vector2Int mazeSize)
    {
        // Make sure that this is not our first time creating the maze.
        this.ClearMaze();

        this.InitMaze(mazeSize);

        MazeGeneratorMod.PopulateMaze(this.Maze);

        this.CreateMazeObjects();
    }


    /// <summary>
    /// Returns true if the position is within the maze size, false otherwise.
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    internal static bool IsPositionValid(int x, int y) => (x < MazeSize.x) && (y < MazeSize.y) && (x >= 0) && (y >= 0);
    internal static bool IsPositionValid(Vector2 pathEnd) => IsPositionValid((int)pathEnd.x, (int)pathEnd.y);
    internal static bool IsPositionValid(Vector2Int pathEnd) => IsPositionValid(pathEnd.x, pathEnd.y);

    /// <summary>
    /// Loads a maze from a script file located in StreamingAssets/Scenarios.
    /// </summary>
    /// <param name="mazeName"></param>
    internal void LoadMaze(string mazeName)
    {
        if (!this.mazeFiles.FilesAreLoaded)
            Debug.LogWarning("Maze manager has not completed loading its files! Please wait for files to load before accessing them!");
        else if (!this.mazeFiles.FileExists(mazeName))
            Debug.LogWarning("The maze " + mazeName + " could not be found! Please check your spelling and double check that the file you want actually exists!");
        else
            this.LoadMaze(this.mazeFiles.LoadFile<MazeLoadData>(mazeName));
    }

    /// <summary>
    /// Converts a MazeLoadData object into a maze data object, then instantiates walls,
    /// mortals, and items in the game world.
    /// </summary>
    /// <param name="maze"></param>
    internal void LoadMaze(MazeLoadData maze)
    {
        // Make sure that this is not our first time creating the maze.
        this.ClearMaze();

        this.InitMaze(maze);

        MazeGeneratorMod.PopulateMaze(this.Maze);

        this.CreateMazeObjects();
    }


    /// <summary>
    /// Returns all walls to the factory and clears the grid.
    /// Does not change the maze size.
    /// </summary>
    internal void ClearMaze()
    {
        for (int x = 0; x < MazeSize.x; x++)
            for (int y = 0; y < MazeSize.y; y++)
                if (this.WallObjects[x, y] != null)
                {
                    WallFactory.Return(this.WallObjects[x, y]);
                    this.WallObjects[x, y] = null;
                }

        this.Maze = null;

        for (int i = 0; i < this.mortals.Count; i++)
            Destroy(this.mortals[i]);
        for (int i = 0; i < this.items.Count; i++)
            Destroy(this.items[i]);
    }

    private void InitMaze(Vector2Int mazeSize)
    {
        this.WallObjects = new Wall[mazeSize.x, mazeSize.y];
        this.Maze = new MazeDescriptor(mazeSize);

        this.Floor.localScale = new Vector3(mazeSize.x / 10f, 1, mazeSize.y / 10f);
        this.Floor.position = new Vector3(mazeSize.x / 2f - 0.5f, 0, mazeSize.y / 2f - 0.5f);
    }

    private void InitMaze(MazeLoadData maze)
    {
        Vector2Int size = new Vector2Int(maze.Maze[0].Length, maze.Maze.Count);
        this.InitMaze(size);

        for (int y = 0; y < maze.Maze.Count; y++)
        {
            // The file is technically read upside-down. Fix it here so the maze matches the file when viewed in a text editor.
            string row = maze.Maze[maze.Maze.Count - 1 - y];
            Assert.AreEqual(row.Length, size.x, "The maze load data does not contain a fixed number of columns! A maze cannot be created from this file!");

            for (int x = 0; x < row.Length; x++)
            {
                char c = row[x];
                // Use Visible flag to determine if the mod scripts should be adding elements to this cell. True means yes.
                if (c == '?')
                    this.Maze[x, y].IsVisible = true;
                else
                {
                    this.Maze[x, y].IsVisible = false;

                    if (c == 'X')
                        this.Maze[x, y].WallExists = true;
                    else if (c == ' ')
                        this.Maze[x, y].WallExists = false;
                    else
                    {
                        foreach (MazeLoadData.MortalDefinition mortal in maze.MortalDefinitions)
                        {
                            if (c == mortal.Key[0])
                            {
                                MortalDescriptor mortalDescriptor = new MortalDescriptor()
                                {
                                    Name = mortal.MortalType,
                                    position = new Vector2(x, y),
                                    TeamId = mortal.TeamId
                                };

                                this.Maze[x, y].MortalsInCell.Add(mortalDescriptor);
                                this.Maze.Mortals.Add(mortalDescriptor);
                            }
                        }
                        foreach (MazeLoadData.ItemDefinition item in maze.ItemDefinitions)
                        {
                            if (c == item.Key[0])
                            {
                                ItemDescriptor itemDescriptor = new ItemDescriptor()
                                {
                                    Name = item.ItemName,
                                    position = new Vector2(x, y)
                                };

                                this.Maze[x, y].ItemsInCell.Add(itemDescriptor);
                                this.Maze.Items.Add(itemDescriptor);
                            }
                        }
                    }
                }

            }            
        }
    }

    /// <summary>
    /// Converts a 2D grid point into a location in 3D world space.
    /// </summary>
    /// <param name="mazeLocation"></param>
    /// <returns>world point</returns>
    internal static Vector3 MazeToWorldSpace(Vector2 mazeLocation) => new Vector3(mazeLocation.x, 0, mazeLocation.y);

    /// <summary>
    /// Converts a point in 3D space into a floating point location on the 2D grid.
    /// </summary>
    /// <param name="worldPoint"></param>
    /// <returns></returns>
    internal static Vector2 WorldToMazeSpace(Vector3 worldPoint)
    {
        Vector2 mazePoint = new Vector2(worldPoint.x, worldPoint.z);

        return new Vector2(Mathf.Clamp(mazePoint.x, 0, MazeSize.x - 1), Mathf.Clamp(mazePoint.y, 0, MazeSize.y - 1));
    }

    /// <summary>
    /// Create the physical maze objects from the data structure.
    /// </summary>
    private void CreateMazeObjects()
    {
        // Create the walls.
        for (int x = 0; x < this.Maze.MazeSize.x; x++)
            for (int y = 0; y < this.Maze.MazeSize.y; y++)
                if (this.Maze[x, y].WallExists)
                    this.WallObjects[x, y] = WallFactory.GetWall(new Vector2Int(x, y), this.MazeParent);

        // Create the other objects.
        foreach (MortalDescriptor mortal in this.Maze.Mortals)
        {
            try
            {
                Transform prefab = this.prefabs[mortal.Name];

                Transform mortalObject = Instantiate(prefab, this.MazeParent);
                mortalObject.position = new Vector3(mortal.position.x, prefab.position.y, mortal.position.y);

                this.mortals.Add(mortalObject.GetComponent<Mortal>());
            }
            catch (KeyNotFoundException)
            {
                Debug.LogError("Failed to instantiate mortal of type " + mortal.Name + "! Is there a typo in the scenario file you are trying to load?");
            }

        }
        foreach (ItemDescriptor item in this.Maze.Items)
        {
            try
            {
                Transform prefab = this.prefabs[item.Name];

                Transform itemObject = Instantiate(prefab, this.MazeParent);
                itemObject.position = new Vector3(item.position.x, prefab.position.y, item.position.y);

                this.items.Add(itemObject.GetComponent<Item>());
            }
            catch (KeyNotFoundException)
            {
                Debug.LogError("Failed to instantiate object of type " + item.Name + "! Is there a typo in the scenario file you are trying to load?");
            }
        }
    }

    internal void OnDestroy()
    {
        // Remove my static reference to me so I can be created again later.
        if (Me == this)
            Me = null;

        // Make sure we don't keep references to any destroyed assets.
        WallFactory.Reset();
    }
}
