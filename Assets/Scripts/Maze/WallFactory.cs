﻿using System;
using UnityEngine;

/// <summary>
/// Creates Walls and maintains the free Wall pool. Also maintains the WallCreation thread.
/// </summary>
[Serializable]
internal sealed class WallFactory : Factory<Wall>
{
    // This class is a singleton. Use Me to get "this"
    private static readonly Lazy<WallFactory> lazy = new Lazy<WallFactory>(() => new WallFactory());
    private static WallFactory Me => lazy.Value;
    private WallFactory() : base() { }

    // Prefab will need to be set before the factory can be used.
    internal static Wall WallPrefab { get => Me.wallPrefab; set => Me.wallPrefab = value; }
    private Wall wallPrefab;

    /// <summary>
    /// Returns a new Wall object.
    /// </summary>
    /// <returns></returns>
    internal static Wall GetWall() => Me.GetFree;
    internal static Wall GetWall(Vector2Int gridLocation, Transform parent)
    {
        Wall wall = GetWall();
        wall.transform.position = MazeManager.MazeToWorldSpace(gridLocation) + new Vector3(0, 0.5f, 0);
        wall.transform.parent = parent;
        wall.gameObject.SetActive(true);
        wall.gameObject.layer = 8;
        return wall;
    }

    internal static void Reset() { Me.ResetFactory(); WallPrefab = null; }
    internal static void Return(Wall wall) => Me.ReturnObj(wall);
    internal static void SetFreeObjectParent(Transform parent) => Me.FreeObjParent = parent;

    /// <summary>
    /// Creates and returns a new Wall object with all components attached. Cannot be directly called,
    /// this function is internal to the parent factory class.
    /// </summary>
    /// <returns></returns>
    protected override Wall CreateNewObject()
    {
        if (this.wallPrefab == null)
        {
            throw new NullPrefabException("Wall factory has no prefab to use!");
        }
        return UnityEngine.Object.Instantiate(this.wallPrefab);
    }
}
