﻿using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;

[assembly: SuppressMessage("Style", "IDE0044:Add readonly modifier", Justification = "Unity accesses private variables during serialization, so they are not truly readonly.")]
[assembly: SuppressMessage("Style", "IDE0051:Private Member is unused", Justification = "Unity accesses private variables during serialization, so they are not truly private.")]

// C# 8.0, we should un-suppress these globally once Unity has an 8.0 combatable update.
[assembly: SuppressMessage("Style", "IDE0054:Use compound assignment", Justification = "Unity doesn't support C# 8.0")]
[assembly: SuppressMessage("Style", "IDE0063:Use simple 'using' statement", Justification = "Unity doesn't support C# 8.0")]
[assembly: SuppressMessage("Style", "IDE0066:Convert switch statement to expression", Justification = "Unity doesn't support C# 8.0")]

// Used to share Start, Update, etc. with unit tests without allowing classes to call each other's Unity funcitons.
[assembly: InternalsVisibleTo("Assembly-CSharp-Editor")]
[assembly: InternalsVisibleTo("Tests_Core")]
[assembly: InternalsVisibleTo("Tests_Integration")]