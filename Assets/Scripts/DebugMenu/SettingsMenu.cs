﻿using UnityEngine;

namespace Debugging
{
    internal class SettingsMenu : DebugMenuPage
    {
        /// <summary>
        /// Defines a list of Terminal Commands relevant to this menu. Can be called from the terminal interface or through a menu interaction.
        /// </summary>
        internal static class TerminalCommands
        {
#pragma warning disable IDE0060 // Remove unused parameter

#pragma warning restore IDE0060 // Remove unused parameter
        }

        /// <summary>
        /// Graphics quality presets
        /// </summary>
        private string[] presets;

        /// <summary>
        /// Music volume
        /// </summary>
        private int musicVolume;

        /// <summary>
        /// Effects volume
        /// </summary>
        private int effectsVolume;

        /// <summary>
        /// Master volume
        /// </summary>
        private int masterVolume;

        /// <summary>
        /// All game resolutions
        /// </summary>
        private Resolution[] allResolutions;

        /// <summary>
        /// The currently selected game resolution
        /// </summary>
        internal int currentResolution;

        /// <summary>
        /// Quality preset
        /// </summary>
        private int curQualityLevel;

        public SettingsMenu()
        {
            this.masterVolume = 100;
            this.effectsVolume = 100;
            this.musicVolume = 100;

            this.presets = QualitySettings.names;
            this.curQualityLevel = QualitySettings.GetQualityLevel();

            this.allResolutions = Screen.resolutions;
            for (int i = 0; i < this.allResolutions.Length; i++)
                if (this.allResolutions[i].Equals(Screen.currentResolution))
                    this.currentResolution = i;
        }

        /// <summary>
        /// The name of the menu, will be displayed in a list of menu tabs before the menu is selected.
        /// </summary>
        public override string Name => "Settings";

        /// <summary>
        /// Draws the menu buttons. Each menu page will define this on its own.
        /// </summary>
        public override void Draw()
        {
            // Two side-by-side menus.
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();

            // Audio settings.
            GUILayout.Label("Audio Settings:");

            GUILayout.Space(32);
            GUILayout.BeginHorizontal();
            GUILayout.Label("Master Volume: " + this.masterVolume);

            this.masterVolume = (int)GUILayout.HorizontalSlider(this.masterVolume, 0, 100);
            GUILayout.EndHorizontal();

            GUILayout.Space(16);
            GUILayout.BeginHorizontal();
            GUILayout.Label("Music Volume: " + this.musicVolume);
            this.musicVolume = (int)GUILayout.HorizontalSlider(this.musicVolume, 0, 100);
            GUILayout.EndHorizontal();

            GUILayout.Space(16);
            GUILayout.BeginHorizontal();
            GUILayout.Label("Effects Volume: " + this.effectsVolume);
            this.effectsVolume = (int)GUILayout.HorizontalSlider(this.effectsVolume, 0, 100);
            GUILayout.EndHorizontal();

            // Move to second menu.
            GUILayout.EndVertical();
            GUILayout.Space(16);
            GUILayout.BeginVertical();

            GUILayout.Label("Graphics Settings:");

            GUILayout.Space(32);

            // Full screen versus windowed.
            if (Screen.fullScreenMode == FullScreenMode.Windowed)
            {
                if (GUILayout.Button("Enable Fullscreen"))
                    Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
            }
            else
            {
                if (GUILayout.Button("Disable Fullscreen"))
                    Screen.fullScreenMode = FullScreenMode.Windowed;
            }

            GUILayout.Space(16);

            // Change screen resolution.
            GUILayout.BeginHorizontal();
            GUILayout.Label("Video resolution:", GUILayout.Width(224));
            if (GUILayout.Button("<", GUILayout.Width(16)) && this.currentResolution > 0)
            {
                this.currentResolution--;
                Screen.SetResolution(this.allResolutions[this.currentResolution].width, this.allResolutions[this.currentResolution].height, (Screen.fullScreenMode != FullScreenMode.Windowed), this.allResolutions[this.currentResolution].refreshRate);
            }
            GUILayout.Label(this.allResolutions[this.currentResolution].ToString(), GUILayout.Width(128));
            if (GUILayout.Button(">", GUILayout.Width(16)) && this.currentResolution < (this.allResolutions.Length - 1))
            {
                this.currentResolution++;
                Screen.SetResolution(this.allResolutions[this.currentResolution].width, this.allResolutions[this.currentResolution].height, (Screen.fullScreenMode != FullScreenMode.Windowed), this.allResolutions[this.currentResolution].refreshRate);
            }
            GUILayout.EndHorizontal();

            GUILayout.Space(32);

            // Single click quality settings (Changes all following settings).
            GUILayout.BeginHorizontal();
            GUILayout.Label("Video Quality Settings:", GUILayout.Width(256));
            if (GUILayout.Button("<", GUILayout.Width(16)) && this.curQualityLevel > 0)
            {
                QualitySettings.DecreaseLevel();
                this.curQualityLevel = QualitySettings.GetQualityLevel();
            }
            GUILayout.Label(this.presets[this.curQualityLevel], GUILayout.Width(64));
            if (GUILayout.Button(">", GUILayout.Width(16)))
            {
                QualitySettings.IncreaseLevel();
                this.curQualityLevel = QualitySettings.GetQualityLevel();
            }
            GUILayout.EndHorizontal();

            GUILayout.Space(16);

            // AA setting.
            GUILayout.BeginHorizontal();
            GUILayout.Label("AA Count:", GUILayout.Width(256));
            if (GUILayout.Button("<", GUILayout.Width(16)) && QualitySettings.antiAliasing > 0)
                QualitySettings.antiAliasing /= 2;
            GUILayout.Label(QualitySettings.antiAliasing.ToString(), GUILayout.Width(64));
            if (GUILayout.Button(">", GUILayout.Width(16)))
                QualitySettings.antiAliasing = (QualitySettings.antiAliasing + 1) * 2;
            GUILayout.EndHorizontal();

            GUILayout.Space(16);

            // Toggle Vsync.
            if (QualitySettings.vSyncCount == 1)
            {
                if (GUILayout.Button("Disable Vsync"))
                    QualitySettings.vSyncCount = 0;
            }
            else
            {
                if (GUILayout.Button("Enable Vsync"))
                    QualitySettings.vSyncCount = 1;
            }

            GUILayout.Space(16);

            // Set the anisotropic filtering setting.
            GUILayout.BeginHorizontal();
            GUILayout.Label("Anisotropic Filtering Settings:", GUILayout.Width(224));
            if (GUILayout.Button("<", GUILayout.Width(16)) && QualitySettings.anisotropicFiltering > AnisotropicFiltering.Disable)
                QualitySettings.anisotropicFiltering--;
            GUILayout.Label(QualitySettings.anisotropicFiltering.ToString(), GUILayout.Width(128));
            if (GUILayout.Button(">", GUILayout.Width(16)) && QualitySettings.anisotropicFiltering < AnisotropicFiltering.ForceEnable)
                QualitySettings.anisotropicFiltering++;
            GUILayout.EndHorizontal();

            // End menu split.
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }
    }
}
