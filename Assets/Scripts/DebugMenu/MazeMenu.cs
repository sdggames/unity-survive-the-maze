﻿using CommandTerminal;
using UnityEngine;

namespace Debugging
{
    internal class MazeMenu : DebugMenuPage
    {
        /// <summary>
        /// Defines a list of Terminal Commands relevant to this menu. Can be called from the terminal interface or through a menu interaction.
        /// </summary>
        internal static class TerminalCommands
        {
#pragma warning disable IDE0060 // Remove unused parameter
            // --------------------------------------------------------------------------------------------------------------------------------------------------
            // Maze Management functions.
            // --------------------------------------------------------------------------------------------------------------------------------------------------
            [RegisterCommand(Name = "NewMaze", Help = "Create a maze of the specified size", MinArgCount = 0, MaxArgCount = 2)]
            private static void NewMaze(CommandArg[] args)
            {
            }

            [RegisterCommand(Name = "ResetMaze", Help = "Removes all maze segments. Does not affect mortals.", MinArgCount = 0, MaxArgCount = 0)]
            private static void ResetMaze(CommandArg[] args)
            {
            }

            [RegisterCommand(Name = "ClearMaze", Help = "Removes all maze segments.", MinArgCount = 0, MaxArgCount = 0)]
            private static void ClearMaze(CommandArg[] args)
            {
            }
#pragma warning restore IDE0060 // Remove unused parameter
        }

        internal MazeManager mazeManager;
        private int mazeSizeX = 50;
        private int mazeSizeY = 50;
        private string mazeToLoad = "Survive";

        public MazeMenu()
        {
        }

        /// <summary>
        /// The name of the menu, will be displayed in a list of menu tabs before the menu is selected.
        /// </summary>
        public override string Name => "Maze Generation";

        /// <summary>
        /// Draws the menu buttons. Each menu page will define this on its own.
        /// </summary>
        public override void Draw()
        {
            if (this.mazeManager == null)
            {
                this.mazeManager = Object.FindObjectOfType<MazeManager>();
                if (this.mazeManager == null)
                    // Could not find the maze manager, don't throw an error, but don't draw anything either.
                    return;
            }

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Create Empty Maze"))
                this.mazeManager.CreateMaze(new Vector2Int(this.mazeSizeX, this.mazeSizeY));

            // Two sliders top and bottom.
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Maze Size: " + this.mazeSizeX, GUILayout.Width(128));
            this.mazeSizeX = (int) GUILayout.HorizontalSlider(this.mazeSizeX, 5, 100);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("by " + this.mazeSizeY, GUILayout.Width(128));
            this.mazeSizeY = (int) GUILayout.HorizontalSlider(this.mazeSizeY, 5, 100);
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            GUILayout.Space(16);

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Load Maze Scenario: " + this.mazeToLoad + ".maze"))
                this.mazeManager.LoadMaze(this.mazeToLoad + ".maze");
            this.mazeToLoad = GUILayout.TextField(this.mazeToLoad);
            GUILayout.EndHorizontal();

            GUILayout.Space(16);

            GUILayout.Label("Maze size: " + this.mazeManager.Maze.MazeSize.ToString());

            GUILayout.BeginVertical();
            // Reverse the y coordinate since we are drawing top to bottom instead of bottom to top.
            for (int y = MazeManager.MazeSize.y - 1; y >= 0; y--)
            {
                GUILayout.BeginHorizontal();
                for (int x = 0; x < MazeManager.MazeSize.x; x++)
                {
                    if (this.mazeManager.Maze[x, y].WallExists)
                        GUILayout.Label( "X", GUILayout.Width(20), GUILayout.Height(20));
                    else if (this.mazeManager.Maze[x,y].MortalsInCell.Count > 0)
                        GUILayout.Label( "0", GUILayout.Width(20), GUILayout.Height(20));
                    else if (this.mazeManager.Maze[x, y].ItemsInCell.Count > 0)
                        GUILayout.Label( "+", GUILayout.Width(20), GUILayout.Height(20));
                    else
                        GUILayout.Label( " ", GUILayout.Width(20), GUILayout.Height(20));
                    GUILayout.Space(-10); // We don't want the standard padding here.
                }

                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
                GUILayout.Space(-10); // We don't want the standard padding here.
            }
            GUILayout.EndVertical();
        }
    }
}
