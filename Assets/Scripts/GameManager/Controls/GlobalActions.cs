﻿using Debugging;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
internal class GlobalActions
{
    [SerializeField] private GameObject light;
    [SerializeField] private List<GenericAction> availableActions;
    [SerializeField] internal List<IKeyPress> alwaysAvailableActions = new List<IKeyPress>();
    private bool ActionsRegistered = false;

    internal GameObject Light { get => this.light; set => this.light = value; }

    internal void RegisterAllActions()
    {
        if (!this.ActionsRegistered)
        {
            this.ActionsRegistered = true;
            this.availableActions = new List<GenericAction>
        {
            new KeyAction
            {
                Action = "Lights",
                Key = KeyCode.L,
                ProcessKeyDown = Lights
            }
        };

            CameraControl cameraControl = UnityEngine.Object.FindObjectOfType<CameraControl>();
            if (cameraControl != null)
            {
                this.alwaysAvailableActions.Add(new KeyAction
                {
                    Action = "Up",
                    Key = KeyCode.UpArrow,
                    ProcessKeyHold = cameraControl.MoveCameraUp
                });
                this.alwaysAvailableActions.Add(new KeyAction
                {
                    Action = "Down",
                    Key = KeyCode.DownArrow,
                    ProcessKeyHold = cameraControl.MoveCameraDown
                });
                this.alwaysAvailableActions.Add(new KeyAction
                {
                    Action = "Left",
                    Key = KeyCode.LeftArrow,
                    ProcessKeyHold = cameraControl.MoveCameraLeft
                });
                this.alwaysAvailableActions.Add(new KeyAction
                {
                    Action = "Right",
                    Key = KeyCode.RightArrow,
                    ProcessKeyHold = cameraControl.MoveCameraRight
                });
            }

            DebugMenuManager debugMenuManager = UnityEngine.Object.FindObjectOfType<DebugMenuManager>();
            if (debugMenuManager != null)
            {
                this.alwaysAvailableActions.Add(new KeyAction
                {
                    Action = "DebugMenu",
                    Key = KeyCode.F3,
                    ProcessKeyDown = debugMenuManager.ToggleMenuVisibility
                });
                this.alwaysAvailableActions.Add(new KeyAction
                {
                    Action = "FpsCounter",
                    Key = KeyCode.F1,
                    ProcessKeyDown = debugMenuManager.ToggleFrameCounterVisibility
                });
            }

            this.alwaysAvailableActions.Add(new KeyAction
            {
                Action = "StartNewGame",
                Key = KeyCode.N,
                ProcessKeyDown = GameManager.NewGame
            });

            //MenuManager menuManager = UnityEngine.Object.FindObjectOfType<MenuManager>();
            //if (menuManager != null)
            //{
            //    alwaysAvailableActions.Add(new KeyAction
            //    {
            //        Action = "QuickMenu",
            //        Key = KeyCode.Escape,
            //        ProcessKeyDown = MenuManager.QuickMenu
            //    });
            //}
        }
    }

    internal GenericAction GetAction(string actionName) => this.availableActions.First(a => a.Action == actionName);

    // -----------------------------------------------------------------------------------------------------
    // Global Key or Mouse actions defined below this point.
    // -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Activates the "Lights" gameobject if it's inactive, and deactivates it if it is active.
    /// </summary>
    internal void Lights()
    {
        if (this.Light != null)
        {
            this.Light.SetActive(!this.Light.activeSelf);
        }
    }
}
