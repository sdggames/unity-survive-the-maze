﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// The Input Manager is the only file that cannot be unit tested.
/// Use for bootstrap code and direct interactions with the keyboard
/// and mouse. Don't make it too complex for simple playtesting!
/// </summary>
internal class InputManager : MonoBehaviour
{
    // Dumb singleton check.
    private static InputManager Me;

    internal void Awake()
    {
        if (Me == null)
            Me = this;
        else
            throw new SingletonReInstantiatedException("There is already a InputManager in the scene!");
    }

    [SerializeField] private float MouseDragStartDistance = 0;
    [SerializeField] private Image MouseDragBox = null;
    [SerializeField] internal TeamInterface PlayerInterface;
    [SerializeField] internal Canvas Canvas;
    [SerializeField] internal ClickIndicatorSettings clickAnimation;
    [SerializeField] internal GlobalActions GlobalActions = new GlobalActions();

    private Vector3 MouseDownPosition;
    private bool IsMouseDragEnabled;
    private RectTransform MouseDragRect;

    private void Start()
    {
        // Enable movement of the mouse drag box.
        if (this.Canvas == null)
            this.Canvas = FindObjectOfType<Canvas>();

        if (this.MouseDragBox != null)
        {
            //We need to reset anchors and pivot to ensure proper positioning
            this.MouseDragRect = this.MouseDragBox.GetComponent<RectTransform>();
            this.MouseDragRect.pivot = Vector2.one * .5f;
            this.MouseDragRect.anchorMin = Vector2.one * .5f;
            this.MouseDragRect.anchorMax = Vector2.one * .5f;
            this.MouseDragBox.gameObject.SetActive(false);
        }

        this.GlobalActions.RegisterAllActions();
    }

    private void Update()
    {
        // Get the left click. This can be used to interact with certain elements.
        if (Input.GetMouseButtonDown(0))
        {
            this.PlayerInterface?.Interact(MouseToGridPosition());

            this.MouseDownPosition = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0)) // Mouse is being held.
        {
            if (Vector2.Distance(Input.mousePosition, this.MouseDownPosition) > this.MouseDragStartDistance)
            {
                this.MouseDragBox.gameObject.SetActive(true);
                this.IsMouseDragEnabled = true;
            }
            if (this.IsMouseDragEnabled)
            {
                this.DrawSquare();
                this.PlayerInterface?.Interact(MouseToGridPosition(this.MouseDownPosition), MouseToGridPosition());
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            this.MouseDragBox.gameObject.SetActive(false);
            this.IsMouseDragEnabled = false;
        }

        // Get the right click. This is used to move players.
        if (Input.GetMouseButtonDown(1))
        {            
            Vector2 gridPosition = MouseToGridPosition();
            ClickIndicator clickAnim = ClickIndicatorFactory.GetClickIndicator(MazeManager.MazeToWorldSpace(gridPosition) + new Vector3(0, 0.1f, 0), this.clickAnimation);
            clickAnim.Animate();

            this.PlayerInterface?.Move(gridPosition);
        }

        if (this.PlayerInterface != null)
            this.ProcessAllKeyActions(this.PlayerInterface.KeyAction);

        this.ProcessAllKeyActions(this.GlobalActions.alwaysAvailableActions);
    }

    /// <summary>
    /// Converts the mouse's current position to a grid-readable location.
    /// Note: This is not an integer array. Cast to Vector2Int to get
    /// the encompasing grid coordinates for this location.
    /// </summary>
    /// <returns>Vector2 Grid coordinates, not rounded</returns>
    internal static Vector2 MouseToGridPosition() => MouseToGridPosition(Input.mousePosition);
    internal static Vector2 MouseToGridPosition(Vector3 mousePosition)
    {
        // Get the location of the mouse on the 2D world plane.
        Ray ray = Camera.main.ScreenPointToRay(mousePosition);
        Plane plane = new Plane(Vector3.up, -0.5f);
        _ = plane.Raycast(ray, out float rayEnter);
        mousePosition = ray.GetPoint(rayEnter);
        // Convert the mouse position to a grid location.
        return MazeManager.WorldToMazeSpace(mousePosition);
    }

    /// <summary>
    /// Process every key action in a key action list.
    /// </summary>
    /// <param name="actions"></param>
    private void ProcessAllKeyActions(List<IKeyPress> actions)
    {
        foreach (KeyAction key in actions)
        {
            if (Input.GetKeyDown(key.Key))
            {
                key.OnKeyDown();
            }
            else if (Input.GetKeyUp(key.Key))
            {
                key.OnKeyUp();
            }

            key.Update();
        }
    }

    /// <summary>
    /// Draws a square on the screen representing a mouse click and drag.
    /// </summary>
    private void DrawSquare()
    {
        Bounds boundingBox = new Bounds
        {
            //The center of the bounds is inbetween startpos and current pos
            center = Vector3.Lerp(this.MouseDownPosition, Input.mousePosition, 0.5f),
            //We make the size absolute (negative bounds don't contain anything)
            size = new Vector3(Mathf.Abs(this.MouseDownPosition.x - Input.mousePosition.x),
            Mathf.Abs(this.MouseDownPosition.y - Input.mousePosition.y),
            0)
        };

        //To display our selectionbox image in the same place as our bounds
        this.MouseDragRect.position = boundingBox.center;
        this.MouseDragRect.sizeDelta = this.Canvas.transform.InverseTransformVector(boundingBox.size);
    }

    internal void OnDestroy()
    {
        // Remove my static reference to me so I can be created again later.
        if (Me == this)
        {
            Me = null;
        }
    }
}
