﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager Me;

    [SerializeField] internal string DefaultMaze = "Survive.maze";
    [SerializeField] internal MazeManager MazeManager;

    internal void Awake()
    {
        if (Me == null)
            Me = this;
        else
            throw new SingletonReInstantiatedException("There is already a GameManager in the scene!");

        if (this.MazeManager == null)
            throw new ObjectNotFoundException("Game Manager cannot find the Maze Manager!");
    }

    /// <summary>
    /// Loads the default maze from file.
    /// </summary>
    /// <param name="mazeName"></param>
    internal static void NewGame() => Me.BootStrap();

    /// <summary>
    /// A temporary function that brings up the maze to the current state of "done."
    /// </summary>
    internal void BootStrap()
    {
        this.MazeManager.LoadMaze(Me.DefaultMaze);

        InputManager inputManager = FindObjectOfType<InputManager>();

        TeamInterface playerTeam = new TeamInterface();
        inputManager.PlayerInterface = playerTeam;

        foreach (Mortal playerMortal in this.MazeManager.mortals)
        {
            if (playerMortal != null && playerMortal.name.Contains("Spartan"))
                playerTeam.AddMortalToTeam(playerMortal);
            else
                playerMortal.TeamId = 1;

            playerMortal.GetComponent<Pathfinding>().SetUpMaze(this.MazeManager.Maze);
        }
    }

    internal void OnDestroy()
    {
        // Remove my static reference to me so I can be created again later.
        if (Me == this)
            Me = null;
    }
}
