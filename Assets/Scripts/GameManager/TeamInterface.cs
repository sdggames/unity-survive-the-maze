﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
internal class TeamInterface
{
    internal TeamInterface() => this.RegisterDefaultKeyActions();

    [SerializeField] private List<Mortal> teamMortals = new List<Mortal>();
    [SerializeField] private List<Mortal> activeMortals = new List<Mortal>();
    [SerializeField] private Dictionary<KeyCode, IKeyPress> keyActions = new Dictionary<KeyCode, IKeyPress>();
    [SerializeField] private List<IMouseClick> mouseActions = new List<IMouseClick>();

    [SerializeField] private float selectionRadius = 2;
    [SerializeField] private int teamId;

    /// <summary>
    /// Returns the list of all Mortals currently ready to accept inputs through this InputInterface.
    /// All Mortals in this list will receive MouseInput and KeyInput commands from the interface.
    /// </summary>
    internal List<Mortal> ActiveMortals => this.activeMortals;

    /// <summary>
    /// Indicates if the "Modify" key is currently held (typically Shift).
    /// </summary>
    internal bool ModifySelect { get; private set; } = false;

    /// <summary>
    /// Returns the TeamId value for the interface and Mortal.
    /// Sets the TeamId value for the InputInterface as well as its Mortal.
    /// </summary>
    internal virtual int TeamId => this.teamId;

    private float SelectionRadius => this.selectionRadius;

    /// <summary>
    /// Returns a readonly list of all keyactions defined by the interface.
    /// CAN NOT BE USED TO ADD KEYACTIONS TO THE INTERFACE. Use AddKeyAction instead.
    /// </summary>
    internal List<IKeyPress> KeyAction => new List<IKeyPress>(this.keyActions.Values);

    /// <summary>
    /// Checks if the interface has defined an action for a particular keycode.
    /// Returns the action if it exists. Throws a KeyNotFoundException if it does not.
    /// </summary>
    /// <param name="keyCode"></param>
    /// <returns></returns>
    internal IKeyPress GetKeyAction(KeyCode keyCode) => this.keyActions[keyCode];
    /// <summary>
    /// Checks if the interface has defined an action for a particular keycode.
    /// Returns the action if it exists. Throws a KeyNotFoundException if it does not.
    /// </summary>
    /// <param name="actionName"></param>
    /// <returns></returns>
    internal IKeyPress GetKeyAction(string actionName)
    {
        KeyAction keyAction = null;

        foreach (KeyAction action in this.keyActions.Values)
        {
            if (action.Action == actionName)
            {
                keyAction = action;
                break;
            }
        }
        if (keyAction == null)
        {
            // No key was found.
            throw new KeyNotFoundException(actionName + " is not a registered key action!");
        }

        return keyAction;
    }

    /// <summary>
    /// Registers the default input keyActions for this interface. For the base class, just registers the shift
    /// keys to "modify" clicks via the ModifySelect bool.
    /// </summary>
    internal virtual void RegisterDefaultKeyActions()
    {
        this.ClearKeyActions();

        // Create keyActions for "shift," used for multiple-select
        KeyAction modify = new KeyAction
        {
            Action = "Shift",
            Key = KeyCode.LeftShift,
            ProcessKeyDown = EnableShift,
            ProcessKeyUp = DisableShift
        };
        this.AddKeyAction(modify);
        KeyAction modify_alt = new KeyAction
        {
            Action = "Shift_Alt",
            Key = KeyCode.RightShift,
            ProcessKeyDown = EnableShift,
            ProcessKeyUp = DisableShift
        };
        this.AddKeyAction(modify_alt);

        // Create key-actions to select an mortal via a keystroke.
        this.AddKeyAction(new KeyAction { Action = "Mortal0", Key = KeyCode.Alpha0, ProcessKeyDown = SelectMortal_0, });
        this.AddKeyAction(new KeyAction { Action = "Mortal1", Key = KeyCode.Alpha1, ProcessKeyDown = SelectMortal_1, });
        this.AddKeyAction(new KeyAction { Action = "Mortal2", Key = KeyCode.Alpha2, ProcessKeyDown = SelectMortal_2, });
        this.AddKeyAction(new KeyAction { Action = "Mortal3", Key = KeyCode.Alpha3, ProcessKeyDown = SelectMortal_3, });
        this.AddKeyAction(new KeyAction { Action = "Mortal4", Key = KeyCode.Alpha4, ProcessKeyDown = SelectMortal_4, });
        this.AddKeyAction(new KeyAction { Action = "Mortal5", Key = KeyCode.Alpha5, ProcessKeyDown = SelectMortal_5, });
        this.AddKeyAction(new KeyAction { Action = "Mortal6", Key = KeyCode.Alpha6, ProcessKeyDown = SelectMortal_6, });
        this.AddKeyAction(new KeyAction { Action = "Mortal7", Key = KeyCode.Alpha7, ProcessKeyDown = SelectMortal_7, });
        this.AddKeyAction(new KeyAction { Action = "Mortal8", Key = KeyCode.Alpha8, ProcessKeyDown = SelectMortal_8, });
        this.AddKeyAction(new KeyAction { Action = "Mortal9", Key = KeyCode.Alpha9, ProcessKeyDown = SelectMortal_9, });

        MouseAction tryActivate = new MouseAction
        {
            Action = "TryActivateMortal",
            ProcessMouseClick = TryActivateMortal,
            ProcessMouseDragRelease = TryActivateMortals
        };
        this.AddMouseAction(tryActivate);
    }

    /// <summary>
    /// Adds the new KeyAction to the interface. Multiple actions cannot share the same key.
    /// Will throw an ArgumentException if the key already exists.
    /// </summary>
    /// <param name="keyAction"></param>
    internal void AddKeyAction(KeyAction keyAction) => this.keyActions.Add(keyAction.Key, keyAction);

    internal void AddKeyActivator(KeyActivator keyActivator)
    {
        this.keyActions.Add(keyActivator.Key, keyActivator);
        this.mouseActions.Add(keyActivator);
    }

    /// <summary>
    /// Adds the new MouseAction to the interface.
    /// </summary>
    /// <param name="mouseAction"></param>
    internal void AddMouseAction(IMouseClick mouseAction) => this.mouseActions.Add(mouseAction);

    /// <summary>
    /// Removes all KeyActions tracked by this interface.
    /// </summary>
    internal void ClearKeyActions() => this.keyActions.Clear();

    /// <summary>
    /// Call from an AI script that wants to simulate a quick key press.
    /// Calls only the ProcessKeyDown delegate. To call other delegates, use OnKeyDown, Update, OnKeyUp.
    /// </summary>
    /// <param name="Key"></param>
    internal void OnKeyTap(KeyCode Key) => this.keyActions[Key].OnKeyTap();
    /// <summary>
    /// Call from an AI script that wants to simulate a quick key press.
    /// Calls only the ProcessKeyDown delegate. To call other delegates, use OnKeyDown, Update, OnKeyUp.
    /// </summary>
    /// <param name="actionName"></param>
    internal void OnKeyTap(string actionName) => this.GetKeyAction(actionName).OnKeyTap();

    /// <summary>
    /// Processes a "down and hold" key event. Each subsequent update will process a key event
    /// until a OnKeyUp event is sent.
    /// </summary>
    /// <param name="Key"></param>
    internal void OnKeyDown(KeyCode Key) => this.keyActions[Key].OnKeyDown();
    /// <summary>
    /// Processes a "down and hold" key event. Each subsequent update will process a key event
    /// until a OnKeyUp event is sent.
    /// </summary>
    /// <param name="actionName"></param>
    internal void OnKeyDown(string actionName) => this.GetKeyAction(actionName).OnKeyDown();

    /// <summary>
    /// Processes a key release. Will trigger an OnKeyUp event, and will stop Update from invoking KeyHeld events.
    /// </summary>
    /// <param name="Key"></param>
    internal void OnKeyUp(KeyCode Key) => this.keyActions[Key].OnKeyUp();
    /// <summary>
    /// Processes a key release. Will trigger an OnKeyUp event, and will stop Update from invoking KeyHeld events.
    /// </summary>
    /// <param name="actionName"></param>
    internal void OnKeyUp(string actionName) => this.GetKeyAction(actionName).OnKeyUp();

    /// <summary>
    /// Turns a left-click from the player or an emulated left-click from a computer player
    /// into a command. Interact commands are "select" or "do thing" style commands.
    /// </summary>
    /// <param name="gridLocation"></param>
    internal virtual void Interact(Vector2 mouseDownLocation)
    {
        foreach (IMouseClick mouseAction in this.mouseActions)
        {
            mouseAction.OnMouseClick(mouseDownLocation);
        }
    }

    /// <summary>
    /// Turns a two point click (mouse down, mouse up) from a player or computer player
    /// into a command. Interact commands are "select" or "do thing" style commands.
    /// </summary>
    /// <param name="mouseDownLocation"></param>
    /// <param name="mouseUpLocation"></param>
    internal virtual void Interact(Vector2 mouseDownLocation, Vector2 mouseUpLocation)
    {
        foreach (IMouseClick mouseAction in this.mouseActions)
        {
            mouseAction.OnMouseDragRelease(mouseDownLocation, mouseUpLocation);
        }
    }

    /// <summary>
    /// Send a command to the Nav Mortal to move to the specified location.
    /// Overridable, but contains basic move and follow functionality.
    /// </summary>
    /// <param name="gridLocation"></param>
    /// <param name="isPlayerInput"></param>
    internal virtual void Move(Vector2 mouseDownLocation)
    {
        if (CharacterManager.DistanceToNearestEnemyMortal(mouseDownLocation, this.TeamId) < this.SelectionRadius)
        {
            foreach (Mortal mortal in this.activeMortals)
                if (mortal != null)
                    mortal.Move.SetTarget(CharacterManager.GetMortal(CharacterManager.GetNearestEnemyMortalId(mouseDownLocation, this.TeamId)));
        }
        else
        {
            if (this.ModifySelect)
            {
                foreach (Mortal mortal in this.activeMortals)
                    if (mortal != null)
                        mortal.Move.QueueDestination(mouseDownLocation);
            }
            else
            {
                foreach (Mortal mortal in this.activeMortals)
                    if (mortal != null)
                        mortal.Move.SetDestination(mouseDownLocation);
            }
        }
    }

    /// <summary>
    /// Processes any held keys. Held and released keys are defined by OnKeyDown and OnKeyUp events.
    /// This class does not check or interact with Unity's Input class directly.
    /// </summary>
    internal void UpdateHeldKeys()
    {
        foreach (KeyAction action in this.KeyAction)
            action.Update();
    }

    /// <summary>
    /// Method set the enable or disable the alternative click action.
    /// </summary>
    private void EnableShift() => this.ModifySelect = true;
    private void DisableShift() => this.ModifySelect = false;

    /// <summary>
    /// Adds a Mortal to the Input Interface and changes the mortal's TeamId to match the
    /// interface's TeamId.
    /// </summary>
    /// <param name="mortal"></param>
    internal void AddMortalToTeam(Mortal mortal)
    {
        mortal.TeamId = this.TeamId;
        this.teamMortals.Add(mortal);
    }

    /// <summary>
    /// Removes all Mortals from this team Interface.
    /// Sets each Mortal's TeamId back to zero.
    /// </summary>
    internal void ClearTeam()
    {
        foreach (Mortal mortal in this.teamMortals)
        {
            if (mortal != null)
                mortal.TeamId = 0;
        }
        this.teamMortals.Clear();
        this.ActiveMortals.Clear();
    }

    /// <summary>
    /// Turns a left-click from the player or an emulated left-click from a computer player
    /// into a command. Interact commands are "select" or "do thing" style commands.
    /// </summary>
    /// <param name="gridLocation"></param>
    internal void TryActivateMortal(Vector2 gridLocation)
    {
        // If we are clicking on a Mortal on my team, we should change to that mortal.
        int nearestMortal = CharacterManager.GetNearestMortalId(gridLocation, this.teamMortals.FindAll(i => i != null).Select(i => i.Id).ToList());

        if (nearestMortal != MortalDescriptor.InvalidId && CharacterManager.DistanceToMortal(gridLocation, nearestMortal) < this.SelectionRadius)
        {
            if (this.ModifySelect)
            {
                if (!this.ActiveMortals.Contains((Mortal)nearestMortal))
                    this.ActiveMortals.Add((Mortal)nearestMortal);
            }
            else
            {
                this.activeMortals.Clear();
                this.activeMortals.Add((Mortal)nearestMortal);
            }
        }
    }

    /// <summary>
    /// Turns a two point click (mouse down, mouse up) from a player or computer player
    /// into a command. Interact commands are "select" or "do thing" style commands.
    /// </summary>
    /// <param name="mouseDownLocation"></param>
    /// <param name="mouseUpLocation"></param>
    internal void TryActivateMortals(Vector2 mouseDownLocation, Vector2 mouseUpLocation)
    {
        // Convert the mouse locations into a rectangle representing the drag.
        Rect mouseDrag = new Rect(
            Mathf.Min(mouseDownLocation.x, mouseUpLocation.x) - 0.001f,
            Mathf.Min(mouseDownLocation.y, mouseUpLocation.y) - 0.001f,
            Mathf.Abs(mouseDownLocation.x - mouseUpLocation.x) + 0.002f,
            Mathf.Abs(mouseDownLocation.y - mouseUpLocation.y) + 0.002f);

        List<Mortal> selectedMortals = new List<Mortal>();

        // Find all mortals on the team that are inside the rectangle.
        foreach (Mortal mortal in this.teamMortals)
        {
            Vector2 mortalPosition = CharacterManager.MortalToGridSpace(mortal.Id);

            if (mouseDrag.Contains(mortalPosition))
            {
                selectedMortals.Add(CharacterManager.GetMortal(mortal.Id));
            }
        }

        // If we are not holding "Shift," then we should select only the mortals in the square.
        if (!this.ModifySelect)
        {
            this.ActiveMortals.Clear();
        }

        foreach (Mortal mortal in selectedMortals)
        {
            if (!this.ActiveMortals.Contains(mortal))
            {
                this.ActiveMortals.Add(mortal);
            }
        }
    }

    /// <summary>
    /// Sets the active mortal by MortalId.
    /// </summary>
    /// <param name="mortal"></param>
    internal void SetActiveMortal(Mortal mortal)
    {
        if (mortal != null)
        {
            this.ActiveMortals.Clear();
            this.ActiveMortals.Add(mortal);

            if (!this.teamMortals.Contains(mortal))
            {
                this.AddMortalToTeam(mortal);
            }
        }
    }

    /// <summary>
    /// Private function to select an mortal via a keyaction press.
    /// </summary>
    /// <param name="mortalIndex"></param>
    private void SelectMortal(int mortalIndex)
    {
        if (this.teamMortals.Count > mortalIndex)
        {
            Mortal mortal = (Mortal)this.teamMortals[mortalIndex];
            if (mortal != null)
            {
                if (this.ModifySelect)
                {
                    if (!this.ActiveMortals.Contains(mortal))
                        this.ActiveMortals.Add(mortal);
                }
                else
                {
                    this.activeMortals.Clear();
                    this.activeMortals.Add(mortal);
                }
            }
        }
    }
    private void SelectMortal_1() => this.SelectMortal(0); private void SelectMortal_2() => this.SelectMortal(1); private void SelectMortal_3() => this.SelectMortal(2);
    private void SelectMortal_4() => this.SelectMortal(3); private void SelectMortal_5() => this.SelectMortal(4); private void SelectMortal_6() => this.SelectMortal(5);
    private void SelectMortal_7() => this.SelectMortal(6); private void SelectMortal_8() => this.SelectMortal(7); private void SelectMortal_9() => this.SelectMortal(8);
    private void SelectMortal_0() => this.SelectMortal(9); // A collection of void delegates all call into SelectMortal. These are passed to KeyAction scripts.
}
