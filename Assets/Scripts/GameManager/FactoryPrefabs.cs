﻿using UnityEngine;

public class FactoryPrefabs : MonoBehaviour
{
    [SerializeField] [Tooltip("A single wall in the maze.")] 
    internal Wall WallPrefab;
    [SerializeField] [Tooltip("Animation that spawns on a right-click")] 
    internal ClickIndicator ClickIndicatorPrefab;

    internal void Awake()
    {
        WallFactory.WallPrefab = this.WallPrefab;
        ClickIndicatorFactory.ClickIndicatorPrefab = this.ClickIndicatorPrefab;

        // Check hookups and throw exceptions if they are missing.
        if (this.WallPrefab == null)
            throw new NullPrefabException("Wall Factory doesn't have a Wall object to clone!");
        if (this.ClickIndicatorPrefab == null)
            throw new NullPrefabException("Click Indicator Factory doesn't have a ClickIndicator object to clone!");
    }
}
