﻿using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

public class ScreenshotManager : MonoBehaviour
{
    public float Progress = 0f;
    public string LastGifFile = "";
    public string LastSnapFile = "";
    public bool IsSaving = false;
    private KeyAction GifKey;
    private KeyAction SnapKey;

    [SerializeField, Tooltip("Output gif width in pixels."), Min(8)]
    private int gifWidth = 320;
    [SerializeField, Tooltip("Output gif height in pixels."), Min(8)]
    private int gifHeight = 200;
    [SerializeField, Tooltip("Automatically compute height from the current aspect ratio.")]
    private bool autoAspect = true;
    [SerializeField, Tooltip("The number of frames per second the gif will run at."), Range(1, 30)]
    private int framePerSecond = 15;
    [SerializeField, Tooltip("-1 to disable, 0 to loop indefinitely, >0 to loop a set number of time."), Min(-1)]
    private int repeat = 0;
    [SerializeField, Tooltip("Lower values mean better quality but slightly longer processing time. 15 is generally a good middleground value."), Range(1, 100)]
    private int quality = 15;
    [SerializeField, Tooltip("The amount of time (in seconds) to record to memory."), Min(0.1f)]
    private float recordingTime = 3f;

    /// <summary>
    /// The Recorder object which actually does the Gif processing.
    /// </summary>
    public Moments.Recorder Recorder;

    /// <summary>
    /// Determines whether the recorder should start on level load or wait for the player to start it manually.
    /// </summary>
    public bool StartImmediately;

    /// <summary>
    /// Output gif width in pixels.
    /// </summary>
    public int GifWidth
    {
        get => this.gifWidth; set
        {
            this.gifWidth = value;
            this.UnappliedSettings = true;

            if (this.autoAspect && Camera.main != null)
              this.gifHeight = Mathf.RoundToInt(value / Camera.main.aspect);
        }
    }

    /// <summary>
    /// Output gif height in pixels.
    /// </summary>
    public int GifHeight
    {
        get => this.gifHeight; set
        {
            this.gifHeight = value;
            this.UnappliedSettings = true;
        }
    }

    /// <summary>
    /// Automatically compute height from the current aspect ratio.
    /// </summary>
    public bool AutoAspect
    {
        get => this.autoAspect; set
        {
            this.autoAspect = value;
            this.UnappliedSettings = true;
        }
    }

    /// <summary>
    /// The number of frames per second the gif will run at.
    /// </summary>
    public int FramePerSecond
    {
        get => this.framePerSecond; set
        {
            this.framePerSecond = value;
            this.UnappliedSettings = true;
        }
    }

    /// <summary>
    /// -1 to disable, 0 to loop indefinitely, >0 to loop a set number of time.
    /// </summary>
    public int Repeat
    {
        get => this.repeat; set
        {
            this.repeat = value;
            this.UnappliedSettings = true;
        }
    }

    /// <summary>
    /// Quality of color quantization (conversion of images to the maximum
    /// 256 colors allowed by the GIF specification). Lower values (minimum = 1) produce better
    /// colors, but slow processing significantly. Higher values will speed up the quantization
    /// pass at the cost of lower image quality (maximum = 100)
    /// </summary>
    public int Quality
    {
        get => this.quality; set
        {
            this.quality = value;
            this.UnappliedSettings = true;
        }
    }

    /// <summary>
    /// "The amount of time (in seconds) to record to memory."
    /// </summary>
    public float RecordingTime
    {
        get => this.recordingTime; set
        {
            this.recordingTime = value;
            this.UnappliedSettings = true;
        }
    }

    /// <summary>
    /// True if settings have been modified, but the recorder has not been restarted. False otherwise.
    /// </summary>
    public bool UnappliedSettings { get; private set; }

    /// <summary>
    /// Returns the estimated memory usage for the current settings.
    /// </summary>
    public float CurrentMemoryUsage => this.Recorder.EstimatedMemoryUse;

    /// <summary>
    /// Returns the estimated memory usage for the new (unapplied settings). Will equal CurrentMemoryUsage if no settings have been modified.
    /// </summary>
    public float EstimatedMemoryUsage => this.framePerSecond * this.RecordingTime * this.gifHeight * this.gifWidth / 262144; // Final constant == 4 / 1024 / 1024

    void Start()
    {
        Assert.IsNotNull(this.Recorder, "Screenshot Manager cannot record Gifs without a Recorder object reference!");

        this.Recorder.OnPreProcessingDone = this.OnProcessingDone;
        this.Recorder.OnFileSaveProgress = this.OnFileSaveProgress;
        this.Recorder.OnFileSaved = this.OnFileSaved;

        this.Recorder.Setup(this.AutoAspect, this.GifWidth, this.GifHeight, this.FramePerSecond, this.RecordingTime, this.Repeat, this.Quality);
        if (this.StartImmediately)
            this.Recorder.Record();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            // Compress & save the buffered frames to a gif file. We should check the State
            // of the Recorder before saving, but for the sake of this example we won't, so
            // you'll see a warning in the console if you try saving while the Recorder is
            // processing another gif.
            this.Recorder.Save();
            this.Progress = 0f;
        }

        if (Input.GetKeyDown(KeyCode.P))
            this.StartCoroutine(this.TakeScreenShot());
    }

    /// <summary>
    /// Applies the settings to the .gif recorder and restarts recording.
    /// </summary>
    public void ApplySettings()
    {
        if (this.Recorder.State != Moments.RecorderState.PreProcessing)
        {
            this.Recorder.Setup(this.AutoAspect, this.GifWidth, this.GifHeight, this.FramePerSecond, this.RecordingTime, this.Repeat, this.Quality);
            this.UnappliedSettings = false;
            this.Recorder.Record();
        }
        else Debug.LogWarning("Wait for the last .gif recording to export before changing settings!");
    }

    private void OnProcessingDone()
    {
        // All frames have been extracted and sent to a worker thread for compression !
        // The Recorder is ready to record again, you can call Record() here if you don't
        // want to wait for the file to be compresse and saved.
        // Pre-processing is done in the main thread, but frame compression & file saving
        // has its own thread, so you can save multiple gif at once.
        this.Recorder.Record();
        this.IsSaving = true;
    }

    private void OnFileSaveProgress(int id, float percent)
    {
        // This callback is probably not thread safe so use it at your own risks.
        // Percent is in range [0;1] (0 being 0%, 1 being 100%).
        this.Progress = percent * 100f;
    }

    private void OnFileSaved(int id, string filepath)
    {
        // Our file has successfully been compressed & written to disk !
        this.LastGifFile = filepath;
        this.IsSaving = false;
    }

    IEnumerator TakeScreenShot()
    {
        yield return new WaitForEndOfFrame();
        ScreenCapture.CaptureScreenshot("SomeLevel.png");
    }
}
