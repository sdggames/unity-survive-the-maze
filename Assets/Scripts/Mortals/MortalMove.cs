﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

internal class MortalMove : MonoBehaviour
{
    [SerializeField] internal Mortal mortal;
    [SerializeField] internal Pathfinding pathfinding;
    [SerializeField] internal float speed;
    [SerializeField] private float retargetFrequency = 5;
    [SerializeField] private float minRetargetTime = 0.33f;
    [SerializeField] private float repathFrequency = 1;
    [SerializeField] private bool walkThroughWalls = false;
    [SerializeField] private Color drawPathColor = Color.yellow;

    private Vector3[] path;
    private int targetIndex;
    private bool idle;

    private Queue<Vector3> Waypoints = new Queue<Vector3>();
    private Mortal Target;
    private Vector2 destination;
    private Vector3 currentWaypoint;
    private Vector3 lastWaypoint;
    private float watchdogTimer;

    internal void Awake()
    {
        Assert.IsNotNull(this.mortal, "The MortalMove Script should point to a Mortal Script!");
        Assert.IsNotNull(this.pathfinding, "The MortalMove Script should point to a Pathfinding Script!");
        this.pathfinding.canTravelThroughWalls = this.walkThroughWalls;
    }

    /// <summary>
    /// Stops all movement and pending movements.
    /// </summary>
    internal void Stop()
    {
        this.CancelInvoke();
        this.path = null;
        this.idle = true;

        // We are not using the waypoints this time, we're going for one target until we reach it.
        this.Waypoints.Clear();
    }

    /// <summary>
    /// Clears any pending waypoints and begins moving the mortal towards the target object.
    /// Will re-target periodically so the mortal continues to follow the target.
    /// </summary>
    /// <param name="target"></param>
    internal void SetTarget(Mortal target)
    {
        if (!this.mortal.Descriptor.HasDied)
        {
            this.Stop();

            this.Target = target;
            this.Retarget();
        }
    }

    /// <summary>
    /// Adds the target vector to a list of waypoints. Waypoints will be visited in order.
    /// </summary>
    /// <param name="targetVector"></param>
    internal void QueueDestination(Vector2 targetVector)
    {
        if (!this.mortal.Descriptor.HasDied)
        {
            this.CancelInvoke();
            this.Target = null;

            // Add our destination to the list.
            if (this.path == null)
                this.SetDestination(targetVector);
            else
                this.Waypoints.Enqueue(targetVector);
        }
    }

    /// <summary>
    /// Clears any pending waypoints and begins moving the mortal to the targetVector.
    /// </summary>
    /// <param name="targetVector"></param>
    internal void SetDestination(Vector2 targetVector)
    {
        if (!this.mortal.Descriptor.HasDied)
        {
            this.CancelInvoke();
            this.Target = null;

            // We are going someplace new, get rid of the old waypoints.
            this.Waypoints.Clear();
            this.Path(targetVector);
        }
    }

    /// <summary>
    /// Start moving towards the destination vector and enable periodic repathing.
    /// </summary>
    /// <param name="destination"></param>
    private void Path(Vector2 destination)
    {
        this.destination = destination;
        this.RePath();
    }

    /// <summary>
    /// Generate a new path to the destination.
    /// </summary>
    private void RePath()
    {
        this.idle = false;

        this.pathfinding.StartFindPath(MazeManager.WorldToMazeSpace(this.transform.position), this.destination, this.OnPathFound);
        if (this.walkThroughWalls)
            this.Invoke(nameof(RePath), this.repathFrequency);
    }

    /// <summary>
    /// Update the path to follow the target.
    /// </summary>
    private void Retarget()
    {
        if (this.Target == null)
        {
            this.Waypoints.Clear();
        }
        else
        {
            // Set my new destination.
            if (this.Target.IsVisible)
            {
                Vector3 position = this.Target.transform.position;
                this.Path(position);

                // Do some math and invoke so we don't spend too much time recalculating a path.
                float timeUntilRepeat = Vector3.Distance(position, this.transform.position) / this.retargetFrequency;
                if (timeUntilRepeat < this.minRetargetTime)
                    timeUntilRepeat = this.minRetargetTime;

                this.Invoke(nameof(Retarget), timeUntilRepeat);
            }
        }
    }

    /// <summary>
    /// Callback when path generation is complete.
    /// </summary>
    /// <param name="newPath"></param>
    /// <param name="pathSuccessful"></param>
    private void OnPathFound(Vector2[] newPath, bool pathSuccessful)
    {
        if (this != null) // This callback could be called on a mortal that has been destroyed.
        {
            if (pathSuccessful && (newPath.Length > 0))
            {
                this.path = newPath.Select(p => new Vector3(p.x, this.transform.position.y, p.y)).ToArray();
                this.targetIndex = 0;
                this.currentWaypoint = this.path[0];
            }
            else if (this.path == null)
            {
                this.idle = true;
            }
        }
    }

    private void Update()
    {
        if (this.path != null)// TODO: && !this.mortal.Descriptor.HasDied)
        {
            if (Vector3.Distance(this.transform.position, this.currentWaypoint) < 0.1f)
            {
                this.targetIndex++;
                if (this.targetIndex >= this.path.Length)
                {
                    if (this.Waypoints.Count > 0)
                        this.Path(this.Waypoints.Dequeue());

                    else
                    {
                        this.path = null;
                        this.idle = true;
                    }
                }
                else
                {
                    this.currentWaypoint = this.path[this.targetIndex];
                }
            }

            this.transform.position = Vector3.MoveTowards(this.transform.position, this.currentWaypoint, this.speed * Time.deltaTime);
            this.transform.LookAt(this.currentWaypoint);
        }

        this.Watchdog();
    }

    private void Watchdog()
    {
        if (this.lastWaypoint == this.currentWaypoint)// TODO: && this.mortal.Descriptor.HasDied == false)
        {
            this.watchdogTimer += Time.deltaTime;
            if (this.watchdogTimer > 3)
            {
                this.Stop();
                this.watchdogTimer = 0;
            }
        }
        else
        {
            this.lastWaypoint = this.currentWaypoint;
            this.watchdogTimer = 0;
        }
    }

    /// <summary>
    /// Show the navigation path.
    /// </summary>
    private void OnDrawGizmos()
    {
        if (this.path != null)
        {
            for (int i = this.targetIndex; i < this.path.Length; i++)
            {
                Gizmos.color = this.drawPathColor;
                Gizmos.DrawCube(this.path[i], new Vector3(0.3f, 0.3f, 0.3f));

                if (i == this.targetIndex)
                {
                    Gizmos.DrawLine(this.transform.position, this.path[i]);
                }
                else
                {
                    Gizmos.DrawLine(this.path[i - 1], this.path[i]);
                }
            }
        }
    }
}
