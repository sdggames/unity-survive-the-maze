﻿using UnityEngine;
using UnityEngine.Assertions;

internal class Mortal : MonoBehaviour
{
    [SerializeField] private MortalDescriptor descriptor = new MortalDescriptor();
    [SerializeField] internal Item mazeItem1;
    [SerializeField] internal Item mazeItem2;
    [SerializeField] internal MortalMove move;

    public float Health;
    public bool IsVisible;

    internal MortalDescriptor Descriptor => this.descriptor;

    /// <summary>
    /// The unique identifier assigned to this Mortal. Will be used to get this mortal from the pool.
    /// </summary>
    internal int Id
    {
        get
        {
            if (this == null) // For Linq, we could ask a null "this" for an Id.
            {
                return MortalDescriptor.InvalidId;
            }
            if (this.descriptor.UniqueMortalId == MortalDescriptor.InvalidId)
            {
                this.descriptor.UniqueMortalId = CharacterManager.RegisterMortal(this);
            }
            return this.descriptor.UniqueMortalId;
        }
    }

    /// <summary>
    /// Sets the team Id for the particular mortal. Mortals that share an ID will appear on the same team and will share visibility.
    /// </summary>
    internal int TeamId { get => this.descriptor.TeamId; set => this.descriptor.TeamId = value; }

    /// <summary>
    /// The movement component for the mortal.
    /// </summary>
    internal MortalMove Move => this.move;

    internal void Awake()
    {
        // Get the ID. If it is not set, it will be registered now.
        _ = this.Id;

        if (this.move == null)
        {
            this.move = this.GetComponent<MortalMove>();
            Assert.IsNotNull(this.move, "There is no move component attached to the object " + this.name + "!");
        }

        this.descriptor.HasDied = false;
    }

    /// <summary>
    /// Succomb to the darkness (or faint at the light).
    /// Disabled mortals can be revived (usually)
    /// </summary>
    internal void Disable()
    {
/* TODO:        if (!this.descriptor.HasDied)
        {
            if (this.mazeItem1 != null)
                this.mazeItem1.Disable();

            if (this.mazeItem2 != null)
                this.mazeItem2.Disable();

            this.descriptor.HasDied = true;
            if (this.Move != null)
                this.Move.Stop();

            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        }
*/    }

    /// <summary>
    /// Brings a mortal back from the darkness.
    /// </summary>
    internal void Revive()
    {
/* TODO:        if (this.descriptor.HasDied)
        {
            if (this.mazeItem1 != null)
                this.mazeItem1.ReEnable();

            if (this.mazeItem2 != null)
                this.mazeItem2.ReEnable();

            this.descriptor.HasDied = false;
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        }
*/    }

    /// <summary>
    /// Convert a mortal Id (int) to a Mortal.
    /// If there is no mortal with that ID, throws an InvalidIndexException.
    /// </summary>
    /// <param name="v"></param>
    public static explicit operator Mortal(int v) => CharacterManager.GetMortal(v);

    private void OnDestroy() => this.Disable();
}
