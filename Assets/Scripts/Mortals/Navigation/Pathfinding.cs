﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading;
using System.Collections.Concurrent;

internal class Pathfinding : MonoBehaviour
{
	/// <summary>
	/// If true, the pathfinding algorithm will path through any unlit wall.
	/// </summary>
	[SerializeField] internal bool canTravelThroughWalls;

	/// <summary>
	/// If true, will not create a path through unlit tiles (will stop when it reaches darkness)
	/// This should be set for any creature that has the ability to see in the dark.
	/// </summary>
	[SerializeField] internal bool stopWhenUnlit;

    // Threading
    private Thread PathFinderAsync { get; set; }
    private AutoResetEvent PathFinderThreadWait { get; set; } = new AutoResetEvent(false);
    private bool PathFinderThreadRunning { get; set; } = false;
    private bool PathFinderThreadHalt { get; set; } = false;

	// Send path requests to the pathfinding thread.
    private ConcurrentQueue<PathRequest> PathGenQueue { get; set; } = new ConcurrentQueue<PathRequest>();
	// Recieve finished paths from the pathfinding thread.
    private ConcurrentQueue<PathRequest> CallbackQueue { get; set; } = new ConcurrentQueue<PathRequest>();
    // The pathfinding grid.
	private Node[,] nodes;

    internal void Awake()
    {
        this.PathFinderThreadRunning = true;
        this.PathFinderAsync = new Thread(this.RunFindPathThread);
    }

	/// <summary>
	/// Set up a tracking array to path through the maze.
	/// </summary>
	/// <param name="maze"></param>
    internal void SetUpMaze(MazeDescriptor maze)
	{
		// Set up a local copy of the maze to use in the pathfinding heap.
		this.nodes = new Node[MazeManager.MazeSize.x, MazeManager.MazeSize.y];

		for (int x = 0; x < MazeManager.MazeSize.x; x++)
			for (int y = 0; y < MazeManager.MazeSize.y; y++)
			{
				Vector2Int position = new Vector2Int(x, y);
				this.nodes[x, y] = new Node(maze[x, y], position);
			}

        this.PathFinderAsync.Start();
	}

	internal void Update()
	{
		if (this.CallbackQueue.TryDequeue(out PathRequest request))
			request.callback(request.path, request.pathFindSuccessful);
	}

	/// <summary>
	/// Start calculating an new path. When the path is complete, a notification will be sent
	/// to the callback function provided.
	/// </summary>
	/// <param name="startPos"></param>
	/// <param name="targetPos"></param>
	/// <param name="callback"></param>
	internal void StartFindPath(Vector2 startPos, Vector2 targetPos, Action<Vector2[], bool> callback)
	{
		PathRequest request = new PathRequest(startPos, targetPos, callback);
		
		// Don't allow more than one outstanding path at a time.
		while (this.PathGenQueue.Count > 0)
			_ = this.PathGenQueue.TryDequeue(out _);

		this.PathGenQueue.Enqueue(request);
		
		_ = this.PathFinderThreadWait.Set();
	}

    private bool Walkable(Node node) => !node.Wall || (this.canTravelThroughWalls && !node.Visible);
	
    private void RunFindPathThread()
    {
		bool dequeueSuccessful;

		while (this.PathFinderThreadRunning)
		{
			dequeueSuccessful = true;
			while (dequeueSuccessful && !this.PathFinderThreadHalt)
			{
				dequeueSuccessful = this.PathGenQueue.TryDequeue(out PathRequest request);
				if (dequeueSuccessful)
				{
                    this.FindPath(request);
				}
			}
			_ = this.PathFinderThreadWait.WaitOne();
			this.PathFinderThreadHalt = false;
		}
	}

	private void FindPath(PathRequest request)
	{
		Node startNode = this.NodeFromPosition(request.pathStart);
		Node endNode = this.NodeFromPosition(request.pathEnd);
		Node targetNode = null;
		bool pathEndValid = MazeManager.IsPositionValid(request.pathEnd);
		request.path = new Vector2[0];

		// Bail if we have an invalid start position (should never happen)
		if (!MazeManager.IsPositionValid(request.pathStart) || !this.Walkable(startNode))
		{
			request.pathFindSuccessful = false;
			this.CallbackQueue.Enqueue(request);
			Debug.LogWarning("An AI attempted to start a path from out-of-bounds or from within a wall!");
			return;
		}

		// Make sure that we have a valid end position.
		if (this.Walkable(endNode))
			targetNode = endNode;
		else 
		{
			// Set our target as the nearest valid neighbor. If none exist, then bail.
			int distanceToStart = int.MaxValue;
			pathEndValid = false;

			foreach (Node neighbour in this.GetNeighbours(endNode))
			{
				int distance = this.GetDistance(neighbour, startNode);
				if (distance < distanceToStart)
					targetNode = neighbour;
			}

			if (targetNode == null)
			{
				request.pathFindSuccessful = false;
				this.CallbackQueue.Enqueue(request);
                Debug.LogWarning("Pathfinding failed to find a valid end node near " + request.pathEnd + "!");
				return;
			}
		}

		Heap<Node> openSet = new Heap<Node>(this.nodes.Length);
		HashSet<Node> closedSet = new HashSet<Node>();
		openSet.Add(startNode);

		while (openSet.Count > 0)
		{
			Node currentNode = openSet.RemoveFirst();
			_ = closedSet.Add(currentNode);

			if (currentNode == targetNode)
			{
				request.pathFindSuccessful = true;
				break;
			}

			foreach (Node neighbour in this.GetNeighbours(currentNode))
			{
				if (!this.Walkable(neighbour) || closedSet.Contains(neighbour))
				{
					if (neighbour == targetNode)
					{
						request.path = this.RetracePath(startNode, currentNode, pathEndValid, request.pathEnd);
						request.pathFindSuccessful = true;
						break;
					}

					continue;
				}

				int newMovementCostToNeighbour = currentNode.gCost + this.GetDistance(currentNode, neighbour);
				if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
				{
					neighbour.gCost = newMovementCostToNeighbour;
					neighbour.hCost = this.GetDistance(neighbour, targetNode);
					neighbour.parent = currentNode;

					if (!openSet.Contains(neighbour))
						openSet.Add(neighbour);
				}
			}
		}

		if (request.pathFindSuccessful && this.Walkable(targetNode))
		{
			request.path = this.RetracePath(startNode, targetNode, pathEndValid, request.pathEnd);
		}

		this.CallbackQueue.Enqueue(request);
	}

	private Vector2[] RetracePath(Node startNode, Node endNode, bool endPositionValid, Vector2 endPosition)
	{
		List<Node> path = new List<Node>();
		Node currentNode = endNode;

		while (currentNode != startNode)
		{
			path.Add(currentNode);
			currentNode = currentNode.parent;
		}

		List<Vector2> waypoints = path.Select(n => new Vector2(n.position.x, n.position.y)).ToList();
		waypoints.Reverse();
		if (endPositionValid)
			waypoints.Add(endPosition);

		return waypoints.ToArray();
	}

	private int GetDistance(Node nodeA, Node nodeB)
	{
		int dstX = Mathf.Abs(nodeA.position.x - nodeB.position.x);
		int dstY = Mathf.Abs(nodeA.position.y - nodeB.position.y);

		if (dstX > dstY)
			return 14 * dstY + 10 * (dstX - dstY);
		return 14 * dstX + 10 * (dstY - dstX);
	}

	private Node NodeFromPosition(Vector2 position)
	{
        return this.nodes[(int)position.x, (int)position.y];
	}

	private List<Node> GetNeighbours(Node node)
	{
		Node[,] neighbours = new Node[3, 3];
		bool invalidNeighbors = false;

		// Build an array of all possible nodes.
		for (int x = -1; x <= 1; x++)
		{
			for (int y = -1; y <= 1; y++)
			{
				if (x == 0 && y == 0)
					continue;

				int checkX = node.position.x + x;
				int checkY = node.position.y + y;

				if (checkX >= 0 && checkX < MazeManager.MazeSize.x && checkY >= 0 && checkY < MazeManager.MazeSize.y)
				{
					neighbours[x + 1, y + 1] = this.nodes[checkX, checkY];
				}
				else
					invalidNeighbors = true;
			}
		}

		// Don't bother with anything fancy if we're at our out of bounds, just go back to a square grid.
		if (invalidNeighbors)
		{
			neighbours[0, 0] = null;
			neighbours[0, 2] = null;
			neighbours[2, 0] = null;
			neighbours[2, 2] = null;
		}
		else
		{
			if (this.canTravelThroughWalls)
			{
				// Remove diagonal nodes that cannot be directly traveled to.
				if (((neighbours[0, 1].Wall) && (neighbours[0, 1].Visible)) || ((neighbours[1, 0].Wall) && (neighbours[1, 0].Visible)))
					neighbours[0, 0] = null;

				if (((neighbours[0, 1].Wall) && (neighbours[0, 1].Visible)) || ((neighbours[1, 2].Wall) && (neighbours[1, 2].Visible)))
					neighbours[0, 2] = null;

				if (((neighbours[2, 1].Wall) && (neighbours[2, 1].Visible)) || ((neighbours[1, 0].Wall) && (neighbours[1, 0].Visible)))
					neighbours[2, 0] = null;

				if (((neighbours[2, 1].Wall) && (neighbours[2, 1].Visible)) || ((neighbours[1, 2].Wall) && (neighbours[1, 2].Visible)))
					neighbours[2, 2] = null;
			}
			else
			{
				// Remove diagonal nodes that cannot be directly traveled to.
				if (neighbours[0, 1].Wall || neighbours[1, 0].Wall)
					neighbours[0, 0] = null;

				if (neighbours[0, 1].Wall || neighbours[1, 2].Wall)
					neighbours[0, 2] = null;

				if (neighbours[2, 1].Wall || neighbours[1, 0].Wall)
					neighbours[2, 0] = null;

				if (neighbours[2, 1].Wall || neighbours[1, 2].Wall)
					neighbours[2, 2] = null;
			}
		}

		List<Node> goodNeighbors = new List<Node>();
		for (int x = 0; x <= 2; x++)
			for (int y = 0; y <= 2; y++)
				if (neighbours[x, y] != null)
					goodNeighbors.Add(neighbours[x, y]);

		return goodNeighbors;
	}

	internal void OnDestroy()
    {
		// If the thread is still running, we should shut it down,
		// otherwise it can prevent the game from exiting correctly.
		this.PathFinderThreadHalt = true;

        if (this.PathFinderThreadRunning)
        {
            // This forces the while loop in the ThreadedWork function to abort.
            this.PathFinderThreadRunning = false;
            // This waits until the thread exits,
            // ensuring any cleanup we do after this is safe. 
            _ = this.PathFinderThreadWait.Set();
            this.PathFinderAsync.Join();
        }
    }

    private struct PathRequest
    {
        internal PathRequest(Vector2 start, Vector2 end, Action<Vector2[], bool> callback)
        {
            this.pathStart = start;
            this.pathEnd = end;
            this.callback = callback;
            this.path = null;
			this.pathFindSuccessful = false;
        }

        // In data
		internal Vector2 pathStart;
        internal Vector2 pathEnd;
        internal Action<Vector2[], bool> callback;
        
		// Out data
		internal Vector2[] path;
		internal bool pathFindSuccessful;
    }
}
