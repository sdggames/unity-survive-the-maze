﻿using UnityEngine;
/// <summary>
/// Pathfinding node used in the A* heap.
/// </summary>
internal class Node : IHeapItem<Node>
{
    internal Node(MazeCellDescriptor mazeCell, Vector2Int position)
    {
        this.mazeCell = mazeCell;
        this.position = position;
    }

    internal bool Wall => this.mazeCell.WallExists;
    internal bool Visible => this.mazeCell.IsVisible;

    internal int gCost;
    internal int hCost;
    internal Vector2Int position;
    internal Node parent;
    private MazeCellDescriptor mazeCell;

    internal int FCost => this.gCost + this.hCost;

    public int HeapIndex { get; set; }

    public int CompareTo(Node nodeToCompare)
    {
        int compare = this.FCost.CompareTo(nodeToCompare.FCost);
        if (compare == 0)
        {
            compare = this.hCost.CompareTo(nodeToCompare.hCost);
        }
        return -compare;
    }
}
