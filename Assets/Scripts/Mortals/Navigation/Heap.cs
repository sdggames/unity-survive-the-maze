﻿using System;

/// <summary>
/// Pathfinding heap used to reduce operation times for the A* algorithm.
/// </summary>
/// <typeparam name="T"></typeparam>
internal class Heap<T> where T : IHeapItem<T>
{
	private T[] items;

	internal Heap(int maxHeapSize) => this.items = new T[maxHeapSize];

	internal void Add(T item)
	{
		item.HeapIndex = this.Count;
		this.items[this.Count] = item;
        this.SortUp(item);
		this.Count++;
	}

	internal T RemoveFirst()
	{
		T firstItem = this.items[0];
		this.Count--;
		this.items[0] = this.items[this.Count];
		this.items[0].HeapIndex = 0;
        this.SortDown(this.items[0]);
		return firstItem;
	}

	internal void UpdateItem(T item) => this.SortUp(item);

    internal int Count { get; private set; }

    internal bool Contains(T item) => Equals(this.items[item.HeapIndex], item);

	private void SortDown(T item)
	{
		while (true)
		{
			int childIndexLeft = item.HeapIndex * 2 + 1;
			int childIndexRight = item.HeapIndex * 2 + 2;
			if (childIndexLeft < this.Count)
			{
				int swapIndex = childIndexLeft;
				if (childIndexRight < this.Count)
				{
					if (this.items[childIndexLeft].CompareTo(this.items[childIndexRight]) < 0)
					{
						swapIndex = childIndexRight;
					}
				}
				if (item.CompareTo(this.items[swapIndex]) < 0)
				{
                    this.Swap(item, this.items[swapIndex]);
				}
				else
				{
					return;
				}
			}
			else
			{
				return;
			}
		}
	}

	private void SortUp(T item)
	{
		int parentIndex = (item.HeapIndex - 1) / 2;

		while (true)
		{
			T parentItem = this.items[parentIndex];
			if (item.CompareTo(parentItem) > 0)
			{
                this.Swap(item, parentItem);
			}
			else
			{
				break;
			}

			parentIndex = (item.HeapIndex - 1) / 2;
		}
	}

	private void Swap(T itemA, T itemB)
	{
		this.items[itemA.HeapIndex] = itemB;
		this.items[itemB.HeapIndex] = itemA;
		int itemAIndex = itemA.HeapIndex;
		itemA.HeapIndex = itemB.HeapIndex;
		itemB.HeapIndex = itemAIndex;
	}
}

internal interface IHeapItem<T> : IComparable<T>
{
	int HeapIndex
	{
		get;
		set;
	}
}
