﻿using System.Collections.Generic;
using UnityEngine;

internal class CharacterManager : MonoBehaviour
{
    private readonly List<Mortal> registeredMortals = new List<Mortal>();

    // Dumb singleton check.
    private static CharacterManager Me;

    internal void Awake()
    {
        if (Me == null)
            Me = this;
        else if (Me != this)
            throw new SingletonReInstantiatedException("There is already a CharacterManager in the scene!");
    }
    
    /// <summary>
    /// Returns the 2D floating location of a Mortal in the maze.
    /// </summary>
    /// <param name="mortalId"></param>
    /// <returns></returns>
    internal static Vector2 MortalToGridSpace(int mortalId) => MazeManager.WorldToMazeSpace(GetMortalPosition(mortalId));
    internal static Vector2 MortalToGridSpace(Mortal mortal) => MazeManager.WorldToMazeSpace(GetMortalPosition(mortal));

    /// <summary>
    /// Returns the Id of the nearest Mortal. The default list of all Mortals are
    /// used in the calculation if no Mortal Id list is given.
    /// </summary>
    /// <param name="gridPosition"></param>
    /// <param name="mortalIds"></param>
    /// <returns></returns>
    internal static int GetNearestMortalId(Vector2 gridPosition, List<int> mortalIds = null)
    {
        if (mortalIds == null)
            mortalIds = GetMortals();

        int nearestMortalId = MortalDescriptor.InvalidId;
        float nearestDistance = float.MaxValue;

        foreach (int mortalId in mortalIds)
        {
            float distance = Vector2.Distance(gridPosition, MortalToGridSpace(mortalId));
            if (distance < nearestDistance)
            {
                nearestDistance = distance;
                nearestMortalId = mortalId;
            }
        }
        return nearestMortalId;
    }
    internal static int GetNearestEnemyMortalId(Vector2 gridPosition, int myTeamId) => GetNearestMortalId(gridPosition, GetAllMortalsNotOnTeam(myTeamId));
    internal static int GetNearestFriendlyMortalId(Vector2 gridPosition, int myTeamId) => GetNearestMortalId(gridPosition, GetAllMortalsOnTeam(myTeamId));

    /// <summary>
    /// Returns the distance to the nearest Mortal. The default list of all Mortals are
    /// used in the calculation if no Mortal Id list or team is specified.
    /// </summary>
    /// <param name="gridPosition"></param>
    /// <param name="mortalIds"></param>
    /// <returns></returns>
    internal static float DistanceToNearestMortal(Vector2 gridPosition, List<int> mortalIds = null)
    {
        if (mortalIds == null || mortalIds.Count > 0)
        {
            return DistanceToMortal(gridPosition, GetNearestMortalId(gridPosition, mortalIds));
        }
        else return float.PositiveInfinity;
    }
    internal static float DistanceToNearestEnemyMortal(Vector2 gridPosition, int myTeamId) => DistanceToNearestMortal(gridPosition, GetAllMortalsNotOnTeam(myTeamId));
    internal static float DistanceToNearestFriendlyMortal(Vector2 gridPosition, int myTeamId) => DistanceToNearestMortal(gridPosition, GetAllMortalsOnTeam(myTeamId));

    /// <summary>
    /// Returns the distance from the gridLocation to the specified Mortal.
    /// Throws an InvalidIndexException if the mortal does not exist.
    /// </summary>
    /// <param name="gridPosition"></param>
    /// <param name="mortalId"></param>
    /// <returns></returns>
    internal static float DistanceToMortal(Vector2 gridPosition, int mortalId) => Vector2.Distance(gridPosition, MortalToGridSpace(mortalId));

    /// <summary>
    /// Returns a list of all Mortal IDs for mortals on team teamId.
    /// </summary>
    /// <param name="teamId"></param>
    /// <returns></returns>
    internal static List<int> GetAllMortalsOnTeam(int teamId)
    {
        List<int> allMortals = GetMortals();
        List<int> mortalsOnTeam = new List<int>();

        foreach (int mortalId in allMortals)
        {
            if (GetMortal(mortalId).TeamId == teamId)
            {
                mortalsOnTeam.Add(mortalId);
            }
        }

        return mortalsOnTeam;
    }

    /// <summary>
    /// Returns a list of all Mortal IDs for mortals NOT on team teamId.
    /// </summary>
    /// <param name="teamId"></param>
    /// <returns></returns>
    internal static List<int> GetAllMortalsNotOnTeam(int teamId)
    {
        List<int> allMortals = GetMortals();
        List<int> mortalsNotOnTeam = new List<int>();

        foreach (int mortalId in allMortals)
        {
            if (GetMortal(mortalId).TeamId != teamId)
            {
                mortalsNotOnTeam.Add(mortalId);
            }
        }

        return mortalsNotOnTeam;
    }

    /// <summary>
    /// Returns the Mortal object that matches the given Id.
    /// If no mortals match the Id, throws an InvalidIndexException
    /// </summary>
    /// <param name="mortalId"></param>
    /// <returns></returns>
    internal static Mortal GetMortal(int mortalId)
    {
        if (Me.registeredMortals.Count <= mortalId || mortalId == MortalDescriptor.InvalidId)
            throw new InvalidIndexException(mortalId.ToString() + " is not a valid MortalId number.");

        return Me.registeredMortals[mortalId];
    }

    /// <summary>
    /// Safely attempts to get the transform.position of the Mortal. If the mortalId is invalid,
    /// returns Vector3.positiveInfinity instead. (This is used because (0,0,0) is a valid position for a Mortal)
    /// </summary>
    /// <param name="mortalId"></param>
    /// <returns></returns>
    internal static Vector3 GetMortalPosition(int mortalId) => GetMortalPosition(GetMortal(mortalId));
    internal static Vector3 GetMortalPosition(Mortal mortal) => (mortal == null) ? Vector3.positiveInfinity : mortal.transform.position;

    /// <summary>
    /// Returns a list of all Mortals 
    /// </summary>
    /// <returns></returns>
    internal static List<int> GetMortals() => Me.GetMortalsPrv();
    private List<int> GetMortalsPrv()
    {
        // Non-static call to reduce "Me" calls.
        List<int> mortalIds = new List<int>();
        foreach (Mortal mortal in Me.registeredMortals)
        {
            if (mortal != null)
            {
                mortalIds.Add(mortal.Id);
            }
        }
        return mortalIds;
    }

    /// <summary>
    /// Returns the Id for a given Mortal. If the mortal is not registered, assign it a new Id.
    /// Also registers the mortal with the GridManager for automatic position updates.
    /// </summary>
    /// <param name="mortal"></param>
    /// <returns></returns>
    internal static int RegisterMortal(Mortal mortal)
    {
        if (Me == null)
            throw new SingletonNotInstantiatedException("There is no Character Manager in the scene!");

        return Me.RegisterMortalPrv(mortal);
    }

    /// <summary>
    /// Adds a new Mortal to the tracking array.
    /// </summary>
    /// <param name="mortal"></param>
    /// <returns></returns>
    private int RegisterMortalPrv(Mortal mortal)
    {
        // Non-static call to reduce "Me" calls.
        // Check that we are truly registered.
        for (int i = 0; i < this.registeredMortals.Count; i++)
        {
            if (mortal == this.registeredMortals[i])
            {
                throw new InvalidIndexException("Nav mortal " + i.ToString() + " has already been registered!");
            }
        }

        this.registeredMortals.Add(mortal);
        return this.registeredMortals.Count - 1;
    }

    /// <summary>
    /// Removes all Mortals from the game as well as from the tracking arrays.
    /// </summary>
    internal static void DestroyAllMortals()
    {
        foreach (Mortal mortal in Me.registeredMortals)
        {
            // Remove them one by one.
            if (mortal != null && mortal.gameObject != null)
            {
                Destroy(mortal.gameObject);
            }
        }
        ClearAllRegisteredMortals();
    }

    /// <summary>
    /// Removes all Mortals from the tracking arrays.
    /// </summary>
    internal static void ClearAllRegisteredMortals() => Me.registeredMortals.Clear();

    internal void OnDestroy()
    {
        // Remove my static reference to me so I can be created again later.
        if (Me == this)
        {
            Me = null;
        }
    }
}
