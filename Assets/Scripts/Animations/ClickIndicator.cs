﻿using UnityEngine;
using ExtraMath;
using System;
using System.Collections;

[Serializable]
internal struct ClickIndicatorSettings
{
    internal enum IndicateMode
    {
        GrowFade,
        ShrinkFade,
        GrowSolid,
        ShrinkSolid
    }

    [SerializeField] internal Color color;
    [SerializeField] internal float fadeOutDuration;
    [SerializeField] internal float finalSize;
    [SerializeField] internal IndicateMode mode;
}

/// <summary>
/// Class used to draw and animate indicator sprites in the maze.
/// </summary>
internal class ClickIndicator : MonoBehaviour
{
    [SerializeField] internal ClickIndicatorSettings settings;
    private SpriteRenderer spriteRenderer;

    private void Awake() => this.spriteRenderer = this.GetComponent<SpriteRenderer>();

    /// <summary>
    /// Animates the ring growing or shrinking.
    /// </summary>
    /// <param name="startDelay"></param>
    internal void Animate(float startDelay = 0) => this.StartCoroutine(this.PlayAnimation(startDelay));

    /// <summary>
    /// Coroutine to animate the ring.
    /// </summary>
    /// <param name="startDelay"></param>
    private IEnumerator PlayAnimation(float startDelay)
    {
        // Make sure that we are not visible until the animation starts.
        this.transform.localScale = Vector3.zero;
        yield return new WaitForSeconds(startDelay);

        // store original color and scale to be able to correctly scale
        float t = 0;
        while (t < this.settings.fadeOutDuration)
        {
            this.SetAlphaAndScale(t / this.settings.fadeOutDuration); // passes value between 0 and 1
            yield return null;
            t += Time.deltaTime;
        }
        this.SetAlphaAndScale(1); // make sure you get the correct final size and color even with imprecise floating point and time
        this.transform.position += Vector3.down;
        ClickIndicatorFactory.Return(this);
    }

    /// <summary>
    /// Sets the current size and color of the ring object.
    /// </summary>
    /// <param name="t"></param>
    private void SetAlphaAndScale(float t)
    {
        switch (this.settings.mode)
        {
            case ClickIndicatorSettings.IndicateMode.GrowFade:
                this.spriteRenderer.color = new Color(this.settings.color.r, this.settings.color.g, this.settings.color.b, MathFx.Sinerp(1, 0, t)); // assign a new color with decreasing alpha
                this.transform.localScale = Vector3.one * Mathf.Lerp(0, this.settings.finalSize, t); // scale relative to original size
                break;
            case ClickIndicatorSettings.IndicateMode.ShrinkFade:
                this.spriteRenderer.color = new Color(this.settings.color.r, this.settings.color.g, this.settings.color.b, MathFx.Sinerp(1, 0, t)); // assign a new color with decreasing alpha
                this.transform.localScale = Vector3.one * Mathf.Lerp(this.settings.finalSize, 0, t); // scale relative to original size
                break;
            case ClickIndicatorSettings.IndicateMode.GrowSolid:
                this.spriteRenderer.color = new Color(this.settings.color.r, this.settings.color.g, this.settings.color.b, this.settings.color.a); // assign a new color with decreasing alpha
                this.transform.localScale = Vector3.one * Mathf.Lerp(0, this.settings.finalSize, t); // scale relative to original size
                break;
            case ClickIndicatorSettings.IndicateMode.ShrinkSolid:
                this.spriteRenderer.color = new Color(this.settings.color.r, this.settings.color.g, this.settings.color.b, this.settings.color.a); // assign a new color with decreasing alpha
                this.transform.localScale = Vector3.one * Mathf.Lerp(this.settings.finalSize, 0, t); // scale relative to original size
                break;
            default:
                throw new InvalidIndexException("Invalid indicator ring animation mode selected!");
        }
    }
}
