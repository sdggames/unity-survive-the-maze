﻿using System;
using UnityEngine;

/// <summary>
/// Creates ClickIndicators and maintains the free ClickIndicator pool. Also maintains the ClickIndicatorCreation thread.
/// </summary>
[Serializable]
internal sealed class ClickIndicatorFactory : Factory<ClickIndicator>
{
    // This class is a singleton. Use Me to get "this"
    private static readonly Lazy<ClickIndicatorFactory> lazy = new Lazy<ClickIndicatorFactory>(() => new ClickIndicatorFactory());
    private static ClickIndicatorFactory Me => lazy.Value;
    private ClickIndicatorFactory() : base() { }

    // Prefab will need to be set before the factory can be used.
    internal static ClickIndicator ClickIndicatorPrefab { get => Me.indicatorPrefab; set => Me.indicatorPrefab = value; }
    private ClickIndicator indicatorPrefab;

    /// <summary>
    /// Returns a new ClickIndicator object.
    /// </summary>
    /// <returns></returns>
    internal static ClickIndicator GetClickIndicator() => Me.GetFree;
    internal static ClickIndicator GetClickIndicator(Vector3 position, ClickIndicatorSettings settings )
    {
        ClickIndicator click = GetClickIndicator();
        click.transform.position = position;
        click.settings = settings;
        return click;
    }

    internal static void Reset() { Me.ResetFactory(); ClickIndicatorPrefab = null; }
    internal static void Return(ClickIndicator click) => Me.ReturnObj(click);
    internal static void SetFreeObjectParent(Transform parent) => Me.FreeObjParent = parent;

    /// <summary>
    /// Creates and returns a new ClickIndicator object with all components attached. Cannot be directly called,
    /// this function is internal to the parent factory class.
    /// </summary>
    /// <returns></returns>
    protected override ClickIndicator CreateNewObject()
    {
        if (this.indicatorPrefab == null)
        {
            throw new NullPrefabException("ClickIndicatorFactory factory has no prefab to use!");
        }
        return UnityEngine.Object.Instantiate(this.indicatorPrefab);
    }
}
