﻿using MoonSharp.Interpreter;
using System;
using System.Collections.Generic;
/// <summary>
/// A descriptor for a single cell in the maze.
/// </summary>
[Serializable] [MoonSharpUserData]
public class MazeCellDescriptor
{
    public MazeCellDescriptor()
    {
        this.ItemsInCell = new List<ItemDescriptor>(1);
        this.MortalsInCell = new List<MortalDescriptor>(2);
    }

    /// <summary>
    /// True if the maze cell is currently illuminated by a light source.
    /// Also used to determine if the mod scripts should fill in this cell with walls. Default true.
    /// </summary>
    public bool IsVisible = true;

    /// <summary>
    /// True if a wall exists in this cell.
    /// </summary>
    public bool WallExists;
    // TODO: WallObject wall

    /// <summary>
    /// A list of any mortals currently passing through this cell. Will return to empty when all mortals leave the cell.
    /// </summary>
    public List<MortalDescriptor> MortalsInCell;

    /// <summary>
    /// A list of any items currently in this cell.
    /// </summary>
    public List<ItemDescriptor> ItemsInCell;
}
