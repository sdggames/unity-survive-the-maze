﻿using MoonSharp.Interpreter;
using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A reference to all data objects for the maze including walls, mortals, and items.
/// The maze size cannot be changed, a new MazeDescriptor should be created when starting a new game.
/// Indexer (maze[x,y]) returns the 
/// </summary>
[Serializable] [MoonSharpUserData]
public class MazeDescriptor
{
    public MazeDescriptor(Vector2Int mazeSize)
    {
        this.MazeSize = mazeSize;
        this.mazeCells = new MazeCellDescriptor[mazeSize.x * mazeSize.y];
        for (int x = 0; x < mazeSize.x; x++)
            for (int y = 0; y < mazeSize.y; y++)
                this.mazeCells[x + (y * mazeSize.x)] = new MazeCellDescriptor();

        this.Mortals = new List<MortalDescriptor>();
        this.Items = new List<ItemDescriptor>();
    }

    /// <summary>
    /// Indexer returns the MazeCell descriptor at the selected maze coordinates.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public MazeCellDescriptor this[int x, int y] => this.mazeCells[x + (y * this.MazeSize.x)];

    /// <summary>
    /// The size of the maze in grid spaces.
    /// </summary>
    public Vector2Int MazeSize { get; }

    /// <summary>
    /// Every mortal in the maze.
    /// </summary>
    public List<MortalDescriptor> Mortals { get; }

    /// <summary>
    /// Every item in the maze that does not fall into a more specific category.
    /// </summary>
    public List<ItemDescriptor> Items { get; }

    [SerializeField] private readonly MazeCellDescriptor[] mazeCells;
}
