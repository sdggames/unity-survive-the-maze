﻿
using MoonSharp.Interpreter;
using System;
/// <summary>
/// A descriptor for a mortal in the maze. 
/// </summary>
[Serializable] [MoonSharpUserData]
public class MortalDescriptor: ItemDescriptor
{
    /// <summary>
    /// An invalid ID for a team or mortal.
    /// </summary>
    public const int InvalidId = -1;

    /// <summary>
    /// The uniwye ID of this mortal.
    /// </summary>
    public int UniqueMortalId = InvalidId;

    /// <summary>
    /// The unique ID of the team that this mortal is on.
    /// </summary>
    public int TeamId;

    /// <summary>
    /// True if the mortal is dead. Dead mortals may be revived.
    /// </summary>
    public bool HasDied;

    /// <summary>
    /// The primary item that the mortal is currently holding. Null means empty.
    /// </summary>
    public ItemDescriptor HeldItem1;

    /// <summary>
    /// The secondary item that the mortal is currently holding. Null means empty.
    /// </summary>
    public ItemDescriptor HeldItem2;
}
