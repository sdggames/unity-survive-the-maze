﻿using System;
using System.Collections.Generic;

[Serializable]
public class MazeLoadData
{
    [Serializable]
    public class MortalDefinition
    {
        public string Key;
        public int TeamId;
        public string MortalType;
    }
    [Serializable]
    public class ItemDefinition
    {
        public string Key;
        public string ItemName;
    }

    public string ScenarioName;
    public string MazeDescription;
    public int NumberOfTeams;
    public List<string> Maze;

    public List<MortalDefinition> MortalDefinitions;
    public List<ItemDefinition> ItemDefinitions;
}
