﻿using MoonSharp.Interpreter;
using System;
using UnityEngine;
/// <summary>
/// A generic descriptor for anything in the maze that is not a wall. 
/// Specific items like mortals or light sources derive from this class.
/// </summary>
[Serializable] [MoonSharpUserData]
public class ItemDescriptor
{
    /// <summary>
    /// The item's name. Must match a prefab object.
    /// </summary>
    public string Name;

    /// <summary>
    /// The item's position in the maze. (0,0) is the lower left corner, cells have a size of 1.
    /// </summary>
    public Vector2 position;

    /// <summary>
    /// True if the item is currently illuminated by a light source.
    /// </summary>
    public bool IsVisible;
}
