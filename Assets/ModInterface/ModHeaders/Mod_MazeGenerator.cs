﻿using Interface;
using UnityEngine;

namespace Callouts
{
    public class MazeGeneratorMod
    {
        public static void PopulateMaze(MazeDescriptor maze) => ModScriptRunner.Call("populateMaze", maze);
    }
}
