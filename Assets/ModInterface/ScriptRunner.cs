﻿using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Loaders;
using MoonSharp.VsCodeDebugger;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ScriptRunner
{
    public ScriptRunner() { }

    public ScriptRunner(string[] ScriptPath) => this.LoadScript(ScriptPath);

    protected Script LuaScripts { get; set; }

    /// <summary>
    /// Load (or reload) the Moon# script. Will not start the script yet.
    /// </summary>
    /// <param name="ScriptPath"></param>
    /// <param name="LoadUnitTests"></param>
    public void LoadScript(string[] ScriptPath)
    {
        // Create a script to run all of our mod code.
        this.LuaScripts = new Script();

        this.LuaScripts.Options.ScriptLoader = new CachedScriptLoader();
        // Set up the session options we need for autoregistration.
        List<string> paths = new List<string>();
        foreach (string script in ScriptPath)
        {
            paths.Add(script + "/?");
            paths.Add(script + "/?.lua");
        }
        ((ScriptLoaderBase)this.LuaScripts.Options.ScriptLoader).ModulePaths = paths.ToArray();
        this.LuaScripts.Options.DebugPrint = Debug.Log;
    }

    /// <summary>
    /// Start the current Moon# Script. Will throw an ArgumentNullException if no script has been loaded.
    /// </summary>
    public void StartScript()
    {
        if (this.LuaScripts == null)
        {
            throw new ArgumentNullException("StartScript called before LoadScript for script runner!");
        }
        _ = this.LuaScripts.DoString(@"require 'LoadScripts'");
    }

    internal void AttachDebugger(MoonSharpVsCodeDebugServer server, int AiNumber) => server.AttachToScript(this.LuaScripts, "AiScript_" + AiNumber.ToString());

    /// <summary>
    /// Calls the specified function.
    /// </summary>
    /// <param name="function">The Lua/MoonSharp function to be called</param>
    /// <returns>
    /// The return value(s) of the function call.
    /// </returns>
    /// <exception cref="ArgumentException">Thrown if function is not of DataType.Function</exception>
    protected DynValue CallMe(string function) => this.LuaScripts.Call(this.LuaScripts.Globals[function]);

    /// <summary>
    /// Calls the specified function.
    /// </summary>
    /// <param name="function">The Lua/MoonSharp function to be called</param>
    /// <param name="args">The arguments to pass to the function.</param>
    /// <returns>
    /// The return value(s) of the function call.
    /// </returns>
    /// <exception cref="ArgumentException">Thrown if function is not of DataType.Function</exception>
    protected DynValue CallMe(string function, params DynValue[] args) => this.LuaScripts.Call(this.LuaScripts.Globals[function], args);

    /// <summary>
    /// Calls the specified function.
    /// </summary>
    /// <param name="function">The Lua/MoonSharp function to be called</param>
    /// <param name="args">The arguments to pass to the function.</param>
    /// <returns>
    /// The return value(s) of the function call.
    /// </returns>
    /// <exception cref="ArgumentException">Thrown if function is not of DataType.Function</exception>
    protected DynValue CallMe(string function, params object[] args) => this.LuaScripts.Call(this.LuaScripts.Globals[function], args);
}
