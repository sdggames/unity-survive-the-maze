﻿using Interface;
using System;
using System.Collections;
using UnityEngine;

public class ModManager : MonoBehaviour
{
    private static ModManager Me;

    private ScriptCache modScripts;
    private ScriptCache aiScripts;
    private bool modScriptsLoaded = false;
    private bool aiScriptsLoaded = false;

    public static ScriptCache ModScripts => Me == null ? null : Me.modScripts;
    public static ScriptCache AiScripts => Me == null ? null : Me.aiScripts;

    public bool ScriptsAreLoaded => this.modScriptsLoaded && this.aiScriptsLoaded;

    internal void Awake()
    {
        if (Me == null)
            Me = this;
        else
            throw new Exception("There is already a GameManager in the scene!");
    }

    internal void Start()
    {
        this.StartCoroutine(this.LoadModScripts());
        this.StartCoroutine(this.LoadAiScripts());
    }

    /// <summary>
    /// Refreshes the script and AI caches to reflect the current mod selection.
    /// Re-starts the mod service. AIs will continue to use the old scripts until the AI expires.
    /// </summary>
    public static void RefreshCache() => _ = Me.StartCoroutine(Me.ReloadScripts());

    /// <summary>
    /// Activates or deactivates the mod. Does not take effect until RefreshCache is called.
    /// </summary>
    /// <param name="modName"></param>
    /// <param name="isActive"></param>
    public static void SetModActive(string modName, bool isActive) => ModScripts.manafest.subDirectoryContents[modName].IsDirectoryActive = isActive;

    /// <summary>
    /// Sets the selected variant for the mod as active, deactivates the rest. Does not take effect until RefreshCache is called.
    /// </summary>
    /// <param name="modName"></param>
    /// <param name="variant"></param>
    public static void SelectModVariant(string modName, string variantName)
    {
        FileManafest mod = ModScripts.manafest.subDirectoryContents[modName];
        foreach (string modVariantName in mod.subDirectories)
        {
            // The tests folder is managed separately.
            if (modVariantName == "Tests" || modVariantName == variantName)
                mod.subDirectoryContents[modVariantName].IsDirectoryActive = true;
            else
                mod.subDirectoryContents[modVariantName].IsDirectoryActive = false;
        }
    }

    /// <summary>
    /// Activates or deactivates the ai. Does not take effect until RefreshCache is called.
    /// </summary>
    /// <param name="modName"></param>
    /// <param name="isActive"></param>
    public static void SetAiActive(string aiName, bool isActive) => AiScripts.manafest.subDirectoryContents[aiName].IsDirectoryActive = isActive;

    /// <summary>
    /// Returns the contents of the requested file. Will return an empty string if the file does not exist.
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public static object LoadFile(string file)
    {
        if (Me.modScripts.FileExists(file))
            return Me.modScripts.LoadFile(file);
        else if (Me.aiScripts.FileExists(file))
            return Me.aiScripts.LoadFile(file);
 
        Debug.LogError("We attempted to load a script before checking if it exists! This shouldn't be possible! File: " + file);
        return "";
    }

    /// <summary>
    /// Looks for a file in the current mod folders and returns true if it exists.
    /// </summary>
    /// <param name="fullPath"></param>
    /// <returns></returns>
    public static bool ScriptFileExists(string file)
    {
        if (Me == null)
        {
            Debug.LogError("Script file requested from cache before the scripts finished loading! Use ModManafer.ScriptsAreLoaded to check if we are ready! File: " + file);
            return false;
        }

        return Me.modScripts.FileExists(file) || Me.aiScripts.FileExists(file);
    }

    /// <summary>
    /// Builds the repository for mod scripts and loads the cache according to the default or saved settings.
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadModScripts()
    {
        this.modScripts = new ScriptCache(ScriptCache.ScriptCacheMode.ModScriptCache);
        yield return this.StartCoroutine(this.modScripts.BuildDirectory());

        // Select the core (default) mod.
        // TODO: Integrate with the save/load manager to keep our custom mod settings across saves.
        foreach (string mod in this.modScripts.manafest.subDirectories)
            SelectModVariant(mod, "Core");

        yield return this.StartCoroutine(this.modScripts.LoadScripts());
        ModScriptRunner.LoadMoonSharp(new ModInterfaceSaveData(), this.modScripts.BuildPaths()); // TODO: Integrate with the settings save/load manager to keep the Debugger setting
        this.modScriptsLoaded = true;
    }

    /// <summary>
    /// Builds the repository for ai scripts and loads the cache according to the default or saved settings.
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadAiScripts()
    {
        this.aiScripts = new ScriptCache(ScriptCache.ScriptCacheMode.AiScriptCache);
        yield return this.StartCoroutine(this.aiScripts.BuildDirectory());
        yield return this.StartCoroutine(this.aiScripts.LoadScripts());
        this.aiScriptsLoaded = true;
    }

    /// <summary>
    /// Refreshes the script and AI caches to reflect the current mod selection.
    /// Re-starts the mod service. AIs will continue to use the old scripts until the AI expires.
    /// </summary>
    /// <returns></returns>
    private IEnumerator ReloadScripts()
    {
        this.modScriptsLoaded = this.aiScriptsLoaded = false;
        yield return this.StartCoroutine(this.modScripts.LoadScripts());
        yield return this.StartCoroutine(this.aiScripts.LoadScripts());
        ModScriptRunner.LoadMoonSharp(new ModInterfaceSaveData(), this.modScripts.BuildPaths()); // TODO: Integrate with the settings save/load manager to keep the Debugger setting
        this.modScriptsLoaded = this.aiScriptsLoaded = true;
    }

    internal void OnDestroy()
    {
        // Remove my static reference to me so I can be created again later.
        if (Me == this)
            Me = null;
    }
}
