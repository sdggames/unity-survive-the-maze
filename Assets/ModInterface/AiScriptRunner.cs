﻿using MoonSharp.Interpreter;
using System.IO;
using System.Threading;
using UnityEngine.Assertions;

namespace Interface
{
    public class AiScriptRunner : ScriptRunner
    {
        private Thread AiRunAsync { get; set; }
        private AutoResetEvent AiThreadWait { get; set; } = new AutoResetEvent(false);
        private bool AiThreadRunning { get; set; } = false;

        public AiScriptRunner(string ScriptPath) : base(new string[] { ScriptPath, Path.Combine("AiScripts", "Ai_Core") }) => this.StartAiThread();

        public void StartAiThread()
        {
            Assert.IsNull(this.AiRunAsync, "Attempted to start an AI script thread twice!");
            this.AiThreadRunning = true;
            this.AiRunAsync = new Thread(this.RunAiThread);
            this.AiRunAsync.Start();
        }

        private void RunAiThread()
        {
            // Wait for the first update.
            _ = this.AiThreadWait.WaitOne();

            while (this.AiThreadRunning)
            {
                _ = this.LuaScripts.Call(this.LuaScripts.Globals["Update"]);
                _ = this.AiThreadWait.WaitOne();
            }
        }

        public void RegisterObject<T>(string Name, T Object)
        {
            _ = UserData.RegisterType<T>();
            this.LuaScripts.Globals[Name] = Object;
        }

        public void Update() => this.AiThreadWait.Set();
    }
}